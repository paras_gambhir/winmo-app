'use strict';
/**
 * Created by shahab on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
    {
        method: 'POST',
        path: '/api/users/register',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.createDriver(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Register Driver ||  WARNING : Will not work from documentation, use postman instead',
            tags: ['api', 'driver'],
            payload: {
                maxBytes: 2000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    name: Joi.string().regex(/^[a-zA-Z ]+$/).trim().required(),
                    countryCode: Joi.string().max(4).required().trim(),
                    phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).required(),
                    facebookId: Joi.string().optional(),
                    email: Joi.string().email().required(),
                    password: Joi.string().required().min(5).trim(),
                    deviceType: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    deviceToken: Joi.string().required().trim(),
                    appVersion: Joi.string().required().trim(),
                    profilePic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/driver/login',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DriverController.loginDriver(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Login Via Email & Password For  Customer',
            tags: ['api', 'driver'],
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().required().min(5).trim(),
                    deviceType: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    deviceToken: Joi.string().required().trim(),
                    flushPreviousSessions: Joi.boolean().required(),
                    appVersion: Joi.string().required().trim()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/driver/changePassword',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.DriverController.changePassword(queryData, userData, function (err, data) {
                console.log('error', err, data)
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'Change Password Driver',
        tags: ['api', 'driver'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                oldPassword: Joi.string().required().min(5).trim(),
                newPassword: Joi.string().required().min(5).trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'PUT',
        path: '/api/driver/resetPassword',
        handler: function (request, reply) {
            var email = request.query.email;
            Controller.DriverController.resetPassword(email, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Reset Password For Customer',
            tags: ['api', 'driver'],
            validate: {
                query: {
                    email: Joi.string().email().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/driver/resendOTP',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.DriverController.resendOTP(userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Resend OTP for Driver',
            tags: ['api', 'driver'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/driver/resendEmailVerificationToken',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.DriverController.resendEmailVerificationToken(userData.email, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Resend Email Verification Token for Driver',
            tags: ['api', 'driver'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/driver/verifyOTP',
        handler: function (request, reply) {
            var OTP = request.query.OTP;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.DriverController.verifyOTP(OTP,userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            description: 'Verify OTP for Driver',
            tags: ['api', 'driver'],
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query: {
                    OTP: Joi.string().length(4).required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/driver/verifyEmail/{emailVerificationToken}',
        handler: function (request, reply) {
            var resetToken = request.params.emailVerificationToken;
            Controller.DriverController.verifyEmail(resetToken, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            description: 'Verify Email for Driver',
            tags: ['api', 'driver'],
            validate: {
                params: {
                    emailVerificationToken: Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/driver/updateBankAccountDetails',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.DriverController.updateBankAccountDetails(userPayload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Driver update bank account details',
            auth: 'UserAuth',
            tags: ['api', 'driver'],
            validate: {
                payload: {
                    CLABE: Joi.string().required().trim().min(2),
                    nameInBankAccount: Joi.string().regex(/^[a-zA-Z ]+$/).trim().required(),
                    routingNumber: Joi.string().required().trim().min(9),
                    accountNumber: Joi.string().required().trim().min(5)
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    /*{
        method: 'PUT',
        path: '/api/driver/updateProfile',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.CustomerController.updateCustomer(userPayload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Customer Update profile || WARNING : Will not work from documentation, use postman instead',
            auth: 'UserAuth',
            tags: ['api', 'driver'],
            payload: {
            maxBytes: 2000000,
            parse: true,
            output: 'file'
            },
            validate: {
            payload: {
                name: Joi.string().optional().allow(''),
                phoneNo: Joi.string().optional().allow(''),
                address: Joi.string().optional().trim().allow(''),
                deviceToken: Joi.string().optional().trim().allow(''),
                profilePic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .description('image file')
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },*/
    {
        method: 'PUT',
        path: '/api/driver/logout',
        handler: function (request, reply) {
            var token = request.auth.credentials.token;
            var userData = request.auth.credentials.userData;
            if (!token){
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN));
            }else if(userData && userData.type != UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER){
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
            }else {
                Controller.DriverController.logoutDriver(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess())
                    }
                });

            }
        },
        config: {
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Logout Driver',
            tags: ['api', 'driver'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }
];