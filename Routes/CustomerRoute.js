'use strict';
/**
 * Created by shumi on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
    {
        method: 'POST',
        path: '/api/customer/loginViaFacebook',
        handler: function (request, reply) {
            var payloadData = request.payload;

            console.log("......request.payload..............",request.payload);

            Controller.CustomerController.loginCustomerViaFacebook(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Login Via Facebook For  Customer',
            tags: ['api', 'customer'],
        
            validate: {
                payload: {
                    facebookId: Joi.string().required(),
                    deviceType: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    deviceToken: Joi.string().required().trim(),
                    appVersion: Joi.string().required().trim(),
                    age:Joi.string().optional().trim(),
                    city:Joi.string().optional().trim(),
                    occupation:Joi.string().optional().trim(),
                    employment:Joi.string().optional().trim(),
                    school:Joi.string().optional().trim(),
                    gender:Joi.string().optional().trim(),
                    name:Joi.string().trim(),
                    facts:Joi.string().trim(),
                    lat:Joi.number().required(),
                    long:Joi.number().required()



                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/profileComplete',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.profileCompleted(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'profile complete',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                age:Joi.string().required(),
                city:Joi.string().required(),
                occupation:Joi.string().required(),
                employment:Joi.string().required(),
                school:Joi.string().required(),
                gender:Joi.string().required(),
                name:Joi.string().trim(),
                facts:Joi.string().trim()

            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/customer/uploadImage',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.uploadImage0(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'uploadImage ',
        tags: ['api', 'customer'],
        payload:{
            maxBytes:10485760,
            parse: true,
            output: 'file',
            allow: 'multipart/form-data'
        },
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                images: Joi.string(),
                images1: Joi.any()
                    .meta({swaggerType: 'file'})
                    .description('image file'),
                images2: Joi.any()
                    .meta({swaggerType: 'file'})
                    .description('image file'),
                images3: Joi.any()
                    .meta({swaggerType: 'file'})
                    .description('image file'),
                images4: Joi.any()
                    .meta({swaggerType: 'file'})
                    .description('image file'),
             },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },


    {
        method: 'POST',
        path: '/api/customer/updateSerachProfile',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.updateSerachProfile(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'update Serach Profile',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                serachMinAge:Joi.string().required(),
                serachGender: Joi.string().required().valid(
                    ['MALE','FEMALE','BOTH']),
                serachMaxAge:Joi.string().required(),
               // searchMinimumDistance:Joi.number().required(),
                searchMaximumDistance:Joi.number().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/users/sendImageMessage',
        config: {
            description: 'sendImageMessage for users',
            tags: ['api', 'users'],
            handler: function (request, reply) {
                var queryData = request.payload;

                Controller.CustomerController.sendImageMessage(queryData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err))
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                })
            },
            payload:{
                maxBytes:100485760,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    accessToken: Joi.string().required(),
                    to:Joi.string().required(),
                    timeStamp:Joi.string().required(),
                    image: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file')
                        .required(),
                    thumbnail: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file'),
                    message_type:Joi.number().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/updateDeviceToken',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.updateDeviceToken(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'update Device Token For users',
            tags: ['api', 'users'],
            validate: {
                payload: {
                    accessToken:Joi.string().required().trim(),
                    deviceType: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    deviceToken: Joi.string().allow(''),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/updateAllFbFriends',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.updateAllFbFriends(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'push all friend fb email id',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                friends: Joi.array().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/updateTempFriends',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.updateTempFriends(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'updateTempFriends',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                friendsId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/getAllFriends',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.getAllFriends(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                        reply(UniversalFunctions.sendSuccess(null, data))                }
            });
        }, config: {
        description: 'get ongoing and frinds list',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                friends: Joi.array().optional()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/checkRegisteredUser',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.checkRegisteredUser(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))                }
            });
        }, config: {
        description: 'get ongoing and frinds list',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                fbId: Joi.string().required(),
                deviceType:Joi.string().required(),
                deviceToken:Joi.string().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/explore',
        handler: function (request, reply) {
            var queryData = request.payload;


            console.log("fjsgkldtfjgjdfklgkd...............",request.payload);

            Controller.CustomerController.explore(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
            reply(UniversalFunctions.sendSuccess(null, data))       
                     }
            });
        }, config: {
        description: 'explore search',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                skip:Joi.number().required(),
                lat:Joi.number().required(),
                long:Joi.number().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/UserDetails',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.UserDetails(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
reply(UniversalFunctions.sendSuccess(null, data))                }
            });
        }, config: {
        description: 'User Details',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                userId:Joi.string().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/getAllGames',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.getAllGames(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        }, config: {
        description: 'get All Games',
        tags: ['api', 'customer'],
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/getAllWager',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.getAllWager(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/requestGames',
        handler: function (request, reply) {
            var queryData = request.payload;

            console.log("........requested games parameter ..........................",request.payload);

            Controller.CustomerController.requestNewGames(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                requestUserId:Joi.string().required(),
                gameId:Joi.string().required(),
                wagerId:Joi.string().required(),
                gameName:Joi.string().required(),
                time:Joi.string().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/requestAccepted',
        handler: function (request, reply) {
            var queryData = request.payload;
            console.log(".......queryData...........request accepted.......",request.payload);
            Controller.CustomerController.requestAccepted(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                gameId:Joi.string().required(),
                senderId:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/sendToOnGoing',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.sendToOnGoing(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
            description: 'send To On Going',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    accessToken: Joi.string().required().trim(),
                    receiver:Joi.string().required(),
                    // senderId:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/setOngoing',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.blockOngoing(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'blockOngoing',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                onGoingId:Joi.string().required(),
                status:Joi.string().required(),

                // senderId:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/customer/requestRejected',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.requestRejected(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
                gameId:Joi.string().required(),
                senderId:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/winGame',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.winnerGames(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                anotherUserId:Joi.string().required(),
                gameId:Joi.string().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/customer/loseGames',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.loseGames(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                anotherUserId:Joi.string().required(),
                gameId:Joi.string().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/customer/imSnake',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.imSnake(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                gameId:Joi.string().required(),
                anotherUserId:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/customer/tieGame',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.tieGame(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                gameId:Joi.string().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },


    {
        method: 'POST',
        path: '/api/customer/rematchGame',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.rematchGame(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                gameId:Joi.string().required(),
                sendToId:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/api/customer/changeWager',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.changeWager(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                gameId:Joi.string().required(),
                wager:Joi.string().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/logout',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.logout(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
            description: 'logout',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    accessToken: Joi.string().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/deleteImage',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.deleteImage(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'deleteImage',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required(),
                imageId: Joi.string().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/getAllChat',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.getAllChat(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT,data))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                accessToken: Joi.string().required().trim(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/getIndivisualChat',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getIndivisualChat(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'getIndivisualChat For users',
            tags: ['api', 'users'],
            validate: {
                payload: {
                    accessToken:Joi.string().required().trim(),
                    anotherUserId:Joi.string().required().trim(),
                    //  skip:Joi.string().required().trim(),
                    id:Joi.string().required().trim()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/guessImage',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.guessImage(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'guessImage For users',
            tags: ['api', 'users'],
            validate: {
                payload: {
                    accessToken:Joi.string().required().trim(),
                    gameId:Joi.string().required().trim()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/customer/testingPush',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CustomerController.testingPush(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'testingPush For users',
            tags: ['api', 'users'],
            validate: {
                query: {
                    //gameId:Joi.string().required().trim()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/customer/gameStatus',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CustomerController.gameStatus(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'gameStatus For users',
            tags: ['api', 'users'],
            validate: {
                query: {
                    accessToken:Joi.string().required().trim(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/customer/rewards',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CustomerController.rewards(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'rewards For users',
            tags: ['api', 'users'],
            validate: {
                query: {
                    accessToken:Joi.string().required().trim(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/customer/pending',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CustomerController.pending(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'pending For users',
            tags: ['api', 'users'],
            validate: {
                query: {
                    accessToken:Joi.string().required().trim(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/accessTokenLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.accessTokenLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'accessTokenLogin For users',
            tags: ['api', 'users'],
            validate: {
                payload: {
                    accessToken:Joi.string().required().trim(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/payWager',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.payWager(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'accessTokenLogin For users',
            tags: ['api', 'users'],
            validate: {
                payload: {
                    accessToken:Joi.string().required().trim(),
                    requestId:Joi.string().required().trim(),
                    cardId:Joi.string().required().trim(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    //{
    //    method: 'POST',
    //    path: '/api/customer/payWager',
    //    handler: function (request, reply) {
    //        var payloadData = request.payload;
    //        Controller.CustomerController.payWager(payloadData, function (err, data) {
    //            if (err) {
    //                reply(UniversalFunctions.sendError(err));
    //            } else {
    //                reply(UniversalFunctions.sendSuccess(null, data))
    //            }
    //        });
    //    },
    //    config: {
    //        description: 'payWager For users',
    //        tags: ['api', 'users'],
    //        validate: {
    //            payload: {
    //                accessToken:Joi.string().required().trim(),
    //                requestId:Joi.string().required().trim(),
    //                cardId:Joi.string().required().trim(),
    //            },
    //            failAction: UniversalFunctions.failActionFunction
    //        },
    //        plugins: {
    //            'hapi-swagger': {
    //                payloadType : 'form',
    //                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
    //            }
    //        }
    //    }
    //},
    {
        method: 'GET',
        path: '/api/customer/getCards',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CustomerController.getCards(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Get Credit Card Info',
        tags: ['api', 'user', 'credit card'],
        validate: {
            query: {
                accessToken:Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType:'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/api/customer/addCard',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.addCard(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Add Credit Card',
        tags: ['api', 'user', 'credit card', 'add'],
        validate: {
            payload: {
                accessToken:Joi.string().required().trim(),
                name:Joi.string().required(),
                cardNumber:Joi.string().required(),
                stripeId:Joi.string().required(),
              //  month:Joi.string().required(),
               // year:Joi.string().required(),
                defaut:Joi.boolean().required(),
                //cvv:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType:'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
      {
        method: 'POST',
        path: '/api/customer/deleteCard',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.deleteCard(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        }, config: {
        description: 'User Delete Credit Card ',
        tags: ['api', 'user', 'credit card','delete'],
        validate: {
            payload: {
                accessToken:Joi.string().required().trim(),
                cardId:Joi.string().required().trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType:'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    //
    //{
    //    method: 'POST',
    //    path: '/api/customer/changeDefaultCard',
    //    handler: function (request, reply) {
    //        var payloadData = request.payload;
    //        Controller.CustomerController.changeDefaultCard(payloadData, function (err, data) {
    //            if (err) {
    //                reply(UniversalFunctions.sendError(err));
    //            } else {
    //                reply(UniversalFunctions.sendSuccess(null, data))
    //            }
    //        });
    //
    //    }, config: {
    //    description: 'User Change Default Credit Card',
    //    tags: ['api', 'user', 'credit card', 'default'],
    //    validate: {
    //        payload: {
    //            accessToken:Joi.string().required().trim(),
    //            cardId:Joi.string().required().trim()
    //        },
    //        failAction: UniversalFunctions.failActionFunction
    //    },
    //    plugins: {
    //        'hapi-swagger': {
    //            payloadType:'form',
    //            responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
    //        }
    //    }
    //}
    //}
/*
    {
        method: 'GET',
        path: '/api/customer/getResetPasswordToken',
        handler: function (request, reply) {
            var email = request.query.email;
            Controller.CustomerController.getResetPasswordToken(email, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Sends Reset Password Token To User',
            tags: ['api', 'customer'],
            validate: {
                query: {
                    email: Joi.string().email().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
   /* {
        method: 'PUT',
        path: '/api/customer/resetPassword',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.resetPassword(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Reset Password For Customer',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    passwordResetToken: Joi.string().required(),
                    newPassword : Joi.string().min(5).required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
   /* {
        method: 'PUT',
        path: '/api/customer/logout',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            if (userData.type == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER){
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));
            }else {
                Controller.CustomerController.logoutCustomer(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess())
                    }
                });
            }
        },
        config: {
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Logout Customer',
            tags: ['api', 'customer'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
/*    {
        method: 'PUT',
        path: '/api/customer/resendOTP',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.CustomerController.resendOTP(userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Resend OTP for Customer',
            tags: ['api', 'customer'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
    /*{
        method: 'PUT',
        path: '/api/customer/verifyOTP',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.CustomerController.verifyOTP(queryData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            description: 'Verify OTP for Customer',
            tags: ['api', 'customer'],
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                payload: {
                    countryCode: Joi.string().max(4).required().trim(),
                    phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).required(),
                    OTPCode: Joi.string().length(4).required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
   /* {
        method: 'PUT',
        path: '/api/customer/verifyEmail/{emailVerificationToken}',
        handler: function (request, reply) {
            var resetToken = request.params.emailVerificationToken;
            Controller.CustomerController.verifyEmail(resetToken, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            description: 'Verify Email for Customer',
            tags: ['api', 'customer'],
            validate: {
                params: {
                    emailVerificationToken: Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
    /*{
        method: 'PUT',
        path: '/api/customer/updateProfile',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.CustomerController.updateCustomer(userPayload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Customer Update profile || WARNING : Will not work from documentation, use postman instead',
            auth: 'UserAuth',
            tags: ['api', 'customer'],
            payload: {
                maxBytes: 2000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    name: Joi.string().optional().min(2),
                    countryCode: Joi.string().max(4).optional().trim(),
                    email: Joi.string().email().optional(),
                    phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).optional(),
                    language: Joi.string().optional().valid([
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.EN,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX]),
                    deviceToken: Joi.string().optional().trim().allow(''),
                    profilePic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
    /*{
        method: 'POST',
        path: '/api/customer/addNewAddress',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.CustomerController.addNewAddress(userPayload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Add New Address For Customer',
            auth: 'UserAuth',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    locationLat : Joi.number().required(),
                    locationLong : Joi.number().required(),
                    streetAddress: Joi.string().required().trim(),
                    state: Joi.string().required().min(2).trim(),
                    apartmentSuite: Joi.string().required().min(1).trim(),
                    nickName: Joi.string().required().min(2).trim(),
                    city: Joi.string().required().min(2).trim(),
                    country: Joi.string().required().min(2).trim(),
                    directionDetails: Joi.string().optional().min(3).trim(),
                    zip: Joi.string().required().min(3).trim()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
   /* {
        method: 'DELETE',
        path: '/api/customer/removeAddress',
        handler: function (request, reply) {
            var addressId = request.query.addressId;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id && addressId) {
                Controller.CustomerController.removeAddress(addressId, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess())
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Remove Address For Customer',
            auth: 'UserAuth',
            tags: ['api', 'customer'],
            validate: {
                query: {
                    addressId : Joi.string().min(3).required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },*/
    /*{
        method: 'GET',
        path: '/api/customer/getAllAddress',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.CustomerController.getAddress(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Get All Addresses of Customer',
            auth: 'UserAuth',
            tags: ['api', 'customer'],
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }*/

    {
        method: 'POST',
        path: '/api/customer/testpush',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.testData(queryData,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'get All Wager',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                token: Joi.string().trim().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
];