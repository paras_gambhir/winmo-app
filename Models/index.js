/**
 * Created by prince
 */
module.exports = {
    Customers : require('./Customers'),
    Images : require('./Images'),
    Admins : require('./Admins'),
    AppVersions : require('./AppVersions'),
    Chats : require('./Chats'),
    RequestGames : require('./RequestGames'),
    RequestMessage : require('./RequestMessage'),
    Wages : require('./Wages'),
    Games : require('./Games'),
    ImageGame : require('./ImageGame')

};