var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');



var Images = new Schema({
    customer: {type: Schema.ObjectId, ref: 'Customers', required: true},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    isDeleted : {type : Boolean, default:false}
});
module.exports = mongoose.model('Images',Images);