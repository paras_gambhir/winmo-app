var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var requestMessage = new Schema({
    sender: {type: Schema.ObjectId, ref: 'Customers', required: true},
    receiver:{type: Schema.ObjectId, ref: 'Customers', required: true},
    is_accpeted:{type:Boolean,default:false},
    requestTime:{type: Date, default:null},
    accpectTime:{type: Date, default:null},
    rejectTime:{type: Date, default:null},
});
module.exports = mongoose.model('requestMessage',requestMessage);