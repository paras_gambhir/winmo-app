var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var requestGames = new Schema({
    sender: {type: Schema.ObjectId, ref: 'Customers', required: true},
    receiver:{type: Schema.ObjectId, ref: 'Customers', required: true},
    matchStatus: {
        type: String, required: true,
        default : "REQUESTED",
        enum: [
            "REQUESTED","LIVE","CLOSE","TIE","REMATCH","REJECT"
        ]
    },
    is_accpeted:{type:Boolean,default:false},
    games:{type: Schema.ObjectId, ref: 'Games', required: true},
    wager:{type: Schema.ObjectId, ref: 'Wagers', required: true},
    oldWager:{type: Schema.ObjectId, ref: 'Wagers',default:null},
    requestTime:{type: Date, default:null},
    accpectTime:{type: Date, default:null},
    rejectTime:{type: Date, default:null},
    gameEndTime:{type: Date, default:null},
    winUser:{type: Schema.ObjectId, ref: 'Customers', default:null},
    lossUser:{type: Schema.ObjectId, ref: 'Customers',default:null},
    payWager:{type:Boolean,default:false},
    snakemark:{type:Boolean,default:false},
    iAmSnake:{type:Boolean,default:false},
    rematchRequest:{type:Boolean,default:false},
    scoreSender:{type:String,default:0},
    scoreReceiver:{type:String,default:0},
    lastMove:{type:String,default:0},
    transactionId:{type:String,default:null},
});
module.exports = mongoose.model('requestGames',requestGames);