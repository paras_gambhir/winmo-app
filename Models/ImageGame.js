var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var ImageGame = new Schema({
    gameId: {type: Schema.ObjectId, ref: 'Games', required: true},
    question: {type: String, default: null},
    questionText: {type: String, default: null},
    optionImage1: {type: String, default: null},
    optionImage2: {type: String, default: null},
    optionImage3: {type: String, default: null},
    optionImage4: {type: String, default: null},
    answer:{type: String, default: null},
    isDeleted:{type:Boolean,default:false}

    //gameSubType: {
    //    type: String, required: true,
    //    default : null,
    //    enum: [
    //        Config.APP_CONSTANTS.DATABASE.GAMESUBTYPE.GUESS_IMAGE,
    //        Config.APP_CONSTANTS.DATABASE.GAMESUBTYPE.ONLY_IMAGE
    //    ]
    //}

});
module.exports = mongoose.model('ImageGame',ImageGame);