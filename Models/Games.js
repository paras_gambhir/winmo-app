var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

//var imageGame = new Schema({
//    optionImage1: {type: String, default: null},
//    optionImage2: {type: String, default: null},
//    optionImage3: {type: String, default: null},
//    optionImage4: {type: String, default: null},
//    answer:{type: String, default: null},
//    gameSubType: {
//        type: String, required: true,
//        default : null,
//        enum: [
//            Config.APP_CONSTANTS.DATABASE.GAMESUBTYPE.GUESS_IMAGE,
//            Config.APP_CONSTANTS.DATABASE.GAMESUBTYPE.ONLY_IMAGE
//        ]
//    },
//});

var Games = new Schema({
    gameName:{type:String,default:null},
    gameImages: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
  //  imageGame:[imageGame],
    gameType: {
        type: String, required: true,
        default : null,
        enum: [
            Config.APP_CONSTANTS.DATABASE.GAMETYPE.TURNBASE,
            Config.APP_CONSTANTS.DATABASE.GAMETYPE.INTERACTIVE
        ]
    },
    uniqueName:{type: String, default: null}
});
module.exports = mongoose.model('Games',Games);