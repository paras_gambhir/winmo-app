var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var onGoing = new Schema({
    customer: {type: Schema.ObjectId, ref: 'Customers', required: true},
    lastGame:{type: Schema.ObjectId, ref: 'games', required: true,default:null},
    lastGameTime:{type: Date, default:null},
    isBlocked:{type:Boolean,default:false,required: true}
});
var friends = new Schema({
    customer: {type: Schema.ObjectId, ref: 'Customers', required: true},
    lastGame:{type: Schema.ObjectId, ref: 'games', required: true,default:null},
    lastGameTime:{type: Date, default:null}
});

var temp = new Schema({
    customer: {type: Schema.ObjectId, ref: 'Customers', required: true},
});

var cards = new Schema({
    name:{type: String, default: null},
    token:{type: String, default: null},
    cardNumber:{type: String, default: null},
    month:{type:Number,default:null},
    year:{type:Number,default:null},
    isDeleted:{type: Boolean, default: false, required: true},
    isDefault:{type: Boolean, default: false, required: true},
    Date: {type: Date, default: Date.now, required: true},
    customerId:{type: String, default: null},
    soruceId:{type: String, default: null},
})


var Customers = new Schema({
    name: {type: String, trim: true, index: true, default: null, sparse: true},
    facebookId: {type: String, default: null, trim: true, index: true},
    gender: {
        type: String,
        default : null,
        enum: [
            Config.APP_CONSTANTS.DATABASE.GENDER.MALE,
            Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE
        ]
    },
    city:{type: String, required: true,default:null},
    occupation:{type: String, required: true,default:null},
    employment:{type: String, required: true,default:null},
    school:{type: String, required: true,default:null},
    facts:{type: String,default:null},
    profileComplete:{type:Boolean,default:false},
    searchFlieds:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false,required: true},
    age:{type:Number, default:0},
    registrationDate: {type: Date, default: Date.now, required: true},
    appVersion: {type: String},
    accessToken: {type: String, trim: true, index: true, unique: true, sparse: true},
    deviceToken: {type: String, trim: true, index: true,sparse: true},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ]
    },
    friends:[friends],
    onGoing:[onGoing],
    temp:[temp],
    cards : [cards],
    currentLocation: {type: [Number], index: '2d'},
    requestGames: [{type: Schema.ObjectId, ref: 'requestGames', required: true,default:null}],
    snake:{type:Number,default:0,required:true},
    lastSnake:{type:Number},
    serachMinAge:{type:Number, default:0},
    serachMaxAge:{type:Number, default:100},
    searchMaximumDistance:{type:Number, default:100},
    serachGender: {
        type: String,default:Config.APP_CONSTANTS.DATABASE.serachGender.BOTH,
        enum: [
            Config.APP_CONSTANTS.DATABASE.serachGender.MALE,
            Config.APP_CONSTANTS.DATABASE.serachGender.FEMALE,
            Config.APP_CONSTANTS.DATABASE.serachGender.BOTH
        ]
    }
});

Customers.index({'currentLocation.coordinates': "2d"});

module.exports = mongoose.model('Customers', Customers);