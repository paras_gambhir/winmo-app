var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var chat = new Schema({
    sender: {type: Schema.ObjectId, ref: 'Customers', required: true},
    receiver:{type: Schema.ObjectId, ref: 'Customers', required: true},
    message:{type:String},
    thumbnail:{type:String},
    sendTime:{type: String, default:null},
    status: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.MEG_STATUS.READ,
            Config.APP_CONSTANTS.DATABASE.MEG_STATUS.REACHSERVER,
            Config.APP_CONSTANTS.DATABASE.MEG_STATUS.DELIVERED
        ]
    },
    message_type:{type:String,default:0},
});
module.exports = mongoose.model('chat',chat);