var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Wagers = new Schema({
    name:{type:String,default:null},
    description:{type:String,default:null},
    images: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    createdBy: {type: Schema.ObjectId, ref: 'Admins', required: true},
    referralCode1:{type:String,default:null, required: true},
    referralCode2:{type:String,default:null, required: true},
    redeemed:{type:Boolean,default:false},
    is_deleted:{type:Boolean,default:false},
    createdAt:{type:String,required: true},
    redeemedAt:{type:Date,default:null},
    redeemedBy:{type: Schema.ObjectId, ref: 'Admins', default:null},
    cost:{type:Number,default:0}
   // count:{type:Number,default:0,required:true}
});
module.exports = mongoose.model('Wagers',Wagers);