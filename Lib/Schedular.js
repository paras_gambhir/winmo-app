/**
 * Created by Happy on 15-01-2017.
 */

var Config = require('../Config');
var async = require('async');
var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var SocketManager = require('../Lib/SocketManager');
var schedule = require('node-schedule');
var NotificationManager = require('../Lib/NotificationManager');


var bootstrapScheduler  = function(){

    var check=0;
    var j = schedule.scheduleJob('0 0 * * *', function () {

        var data;
        //    console.log("entered in scheduler");
      //  var date = new Date().se

        var d = new Date().getUTCMonth();

        var date = new Date().setUTCMonth(d-i);

        date = new Date(date).setUTCHours(0);

        date = new Date(date).setUTCMinutes(0);

        date = new Date(date).setUTCSeconds(0);
        async.auto({
            getAllCustomers:function(cb){
                var query = {
                    snake:{gte:5},
                    lastSnake:{lte:date}
                };
                var dataToGet = {
                    _id:1
                };
                Service.CustomerService.getCustomer(query,dataToGet,{},function(err,result){
                    if(err){
                        console.log(err);
                        cb(err)
                    }
                    else{
                        data = result;
                        cb(null);
                    }
                })
            },
            updateSnakeCount:['getAllCustomers',function(cb){
                if(data.length){

                    for(var i=0;i<data.length;i++){
                        (function(i){
                            var query = {
                                _id:data[i]._id
                                //  lastSnake:
                            };
                            var dataToGet = {
                                snake:0
                            };
                            Service.CustomerService.updateCustomer(query,dataToGet,{},function(err,result){
                                if(err){
                                    console.log(err);
                                    cb(err)
                                }
                                else{
                                   if(i==data.length-1)
                                    cb(null);
                                }
                            })
                        }(i))
                    }
                }
                else{
                    cb(null)
                }
            }]
        }, function (err, result) {
            if (err) {
                console.log(".............in err.....................");
            } else {
                //       console.log(".......................in result.....................");
            }
        })
    })
};





module.exports = {
    bootstrapScheduler:bootstrapScheduler,
};