'use strict';
/**
 * Created by shumi on 12/7/15.
 */
var Config = require('../Config');
var async = require('async');
//var gcm = require('node-gcm');
var apn = require("apn");
var client = require('twilio')(Config.smsConfig.twilioCredentials.accountSid, Config.smsConfig.twilioCredentials.authToken);
var nodeMailerModule = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodeMailerModule.createTransport(smtpTransport(Config.emailConfig.nodeMailer.Mandrill));
var FCM = require('fcm-push');

var serverKey = 'AIzaSyDYCNhydSZzfvP7zoWYnmGE4MTw3s3IecQ';

var fcm = new FCM(serverKey);

var sendPUSHToUser = function (pushToSend, callback) {
    callback();
};

var sendSMSToUser = function (four_digit_verification_code, countryCode, phoneNo, externalCB) {
    console.log('sendSMSToUser')

    var templateData = Config.APP_CONSTANTS.notificationMessages.verificationCodeMsg;
    var variableDetails = {
        four_digit_verification_code: four_digit_verification_code
    };

    var smsOptions = {
        from: Config.smsConfig.twilioCredentials.smsFromNumber,
        To: countryCode + phoneNo.toString(),
        Body: null
    };

    async.series([
        function (internalCallback) {
            smsOptions.Body = renderMessageFromTemplateAndVariables(templateData, variableDetails);
            internalCallback();
        }, function (internalCallback) {
            sendSMS(smsOptions, function (err, res) {
                internalCallback(err, res);
            })
        }
    ], function (err, responses) {
        if (err) {
            externalCB(err);
        } else {
            externalCB(null, Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT);
        }
    });
};


var sendEmailToUser = function (emailType ,emailVariables, emailId, callback) {
    var mailOptions = {
        from: 'support@boohol.com',
        to: emailId,
        subject: null,
        html: null
    };
    async.series([
        function(cb){
            switch (emailType){
                case 'REGISTRATION_MAIL' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.registrationEmail.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.registrationEmail.emailMessage, emailVariables) ;
                    break;
                case 'FORGOT_PASSWORD' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.forgotPassword.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.forgotPassword.emailMessage, emailVariables) ;
                    break;
                case 'DRIVER_CONTACT_FORM' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.contactDriverForm.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.contactDriverForm.emailMessage, emailVariables) ;
                    break;
                case 'BUSINESS_CONTACT_FORM' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.contactBusinessForm.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.contactBusinessForm.emailMessage, emailVariables) ;
                    break;
            }
            cb();

        },function(cb){
            sendMailViaTransporter(mailOptions, function(err,res){
                cb(err,res);
            })
        }
    ], function (err, responses) {
        if (err){
            callback(err);
        }else {
            callback();
        }
    });

};

function renderMessageFromTemplateAndVariables(templateData, variablesData) {
    var Handlebars = require('handlebars');
    return Handlebars.compile(templateData)(variablesData);
}

/*
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @ sendSMS Function
 @ This function will initiate sending sms as per the smsOptions are set
 @ Requires following parameters in smsOptions
 @ from:  // sender address
 @ to:  // list of receivers
 @ Body:  // SMS text message
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */
function sendSMS(smsOptions, cb) {
    client.messages.create(smsOptions, function (err, message) {
        console.log('SMS RES', err, message);
        if (err) {
            console.log(err)
        }
        else {
            console.log(message.sid);
        }
    });
    cb(null, null); // Callback is outside as sms sending confirmation can get delayed by a lot of time
}


/*
 ==========================================================
 Send the notification to the iOS device for customer
 ==========================================================
 */

function sendIosPushNotification(iosDeviceToken, message,payload,cb) {

    var debugging_enabled=1;

    var path;
        path =__dirname + "/WinmoDeveloper.pem";

    var DEBUG=apn;
    var status = 1;
    var msg = message;
    var snd = 'ping.aiff';

    var options = {
        cert: path ,
        certData: null,
        key:path,
        keyData: null,
        passphrase: 'winmo',
        ca: null,
        pfx: null,
        pfxData: null,
        gateway: 'gateway.sandbox.push.apple.com',
        port: 2195,
        rejectUnauthorized: true,
        enhanced: true,
        cacheLength: 100,
        autoAdjustCache: true,
        connectionTimeout: 0,
        ssl: true,
        production:true
    };

    console.log("==============here==============",iosDeviceToken)

    var apnConnection = new apn.Connection(options);

    var deviceToken = new apn.Device(iosDeviceToken);

    var note = new apn.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 1;
    note.sound = "ping.aiff";
    note.alert = payload;
    note.payload = message;
    // note.device =deviceToken
    //console.log(note)
    apnConnection.pushNotification(note,deviceToken);


    function log(type) {
        return function () {
            if (DEBUG)
                console.log("iOS PUSH NOTIFICATION RESULT: " + type);
        }
    }
    cb(null);

    apnConnection.on('error', log('error'));
    apnConnection.on('transmitted', log('transmitted'));
    apnConnection.on('timeout', log('timeout'));
    apnConnection.on('connected', log('connected'));
    apnConnection.on('socketError', log('socketError'));
    apnConnection.on('cacheTooSmall', log('cacheTooSmall'));

}


/*
 ==============================================
 Send the notification to the android device
 =============================================
 */
function sendAndroidPushNotification(deviceToken,dataMessage,callback) {


    var message = {
        to: deviceToken, // required fill with device token or topics
        collapse_key: 'your_collapse_key',

        data: {
            title: 'Title of your push notification',
            body: dataMessage
        }
    };
    fcm.send(message, function(err, response){
        if (err) {
            callback(err)
            console.log("Something has gone wrong!",err);
        } else {
            callback(null)
            console.log("Successfully sent with response: ", response);
        }
    });
}

/*
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @ sendMailViaTransporter Function
 @ This function will initiate sending email as per the mailOptions are set
 @ Requires following parameters in mailOptions
 @ from:  // sender address
 @ to:  // list of receivers
 @ subject:  // Subject line
 @ html: html body
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */
function sendMailViaTransporter(mailOptions, cb) {
    transporter.sendMail(mailOptions, function (error, info) {
        console.log('Mail Sent Callback Error:',error);
        console.log('Mail Sent Callback Ifo:',info);
    });
    cb(null, null) // Callback is outside as mail sending confirmation can get delayed by a lot of time
}

module.exports = {
    sendSMSToUser: sendSMSToUser,
    sendEmailToUser: sendEmailToUser,
    sendPUSHToUser: sendPUSHToUser,
    sendIosPushNotification:sendIosPushNotification,
    sendAndroidPushNotification:sendAndroidPushNotification
};