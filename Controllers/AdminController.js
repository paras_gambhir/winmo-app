'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var emailPassword = require('../Lib/email');
var Config = require('../Config');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');

var updateCustomer = function (phoneNo, data, callback) {
    var criteria = {
        phoneNo: phoneNo
    };
    var dataToSet = {};
    if (data.name) {
        dataToSet.name = data.name;
    }
    if (data.email) {
        dataToSet.email = data.email;
    }
    if (data.phoneNo) {
        dataToSet.phoneNo = data.phoneNo;
    }
    if (data.deviceToken) {
        dataToSet.deviceToken = data.deviceToken;
    }
    if (data.appVersion) {
        dataToSet.appVersion = data.appVersion;
    }
    if (data.deviceType) {
        dataToSet.deviceType = data.deviceType;
    }
    if (data.hasOwnProperty('isBlocked')) {
        dataToSet.isBlocked = data.isBlocked;
    }
    if (data.hasOwnProperty('defaultCheckoutOption')) {
        dataToSet.defaultCheckoutOption = data.defaultCheckoutOption;
    }
    var options = {
        new: true
    };
    Service.CustomerService.updateCustomer(criteria, dataToSet, options, function (err, data) {
        if (err) {
            callback(err)
        } else {
            if (data) {
                callback(null, data)
            } else {
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
            }
        }
    })
};

var resetPassword = function (email, callback) {
    var generatedPassword = UniversalFunctions.generateRandomString();
    var customerObj = null;
    if (!email) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    email: email
                };
                var setQuery = {
                    firstTimeLogin: true,
                    password: UniversalFunctions.CryptData(generatedPassword)
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, userData) {
                    console.log('update customer', err, userData)
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            customerObj = userData;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                if (customerObj) {
                    var variableDetails = {
                        user_name: customerObj.name,
                        password_to_login: generatedPassword
                    };
                    NotificationManager.sendEmailToUser(variableDetails, customerObj.email, cb)
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            callback(err, {generatedPassword: generatedPassword}); //TODO Change in production DO NOT Expose the password
        })
    }
};

var adminLogin = function(userData, callback) {

    var tokenToSend = null;
    var responseToSend = {};
    var tokenData = null;
console.log(userData.adminType)
    async.series([
        function (cb) {
        var getCriteria = {
            email: userData.email,
            password: UniversalFunctions.CryptData(userData.password),
            adminType:userData.adminType
        };
       // console.log(UniversalFunctions.CryptData(userData.password))
        Service.AdminService.getAdmin(getCriteria, {}, {}, function (err, data) {
            if (err) {
                console.log(err)
                cb({errorMessage: 'DB Error: ' + err})
            } else {
                if (data && data.length > 0 && data[0].email) {
                        console.log(data)

                    tokenData = {
                        id: data[0]._id,
                        username: data[0].username,
                        type : UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN
                    };
                    cb()
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS)
                }
            }
        });
    }, function (cb) {
        var setCriteria = {
            email: userData.email
        };
        var setQuery = {
            $push: {
                loginAttempts: {
                    validAttempt: (tokenData != null),
                    ipAddress: userData.ipAddress
                }
            }
        };
        Service.AdminService.updateAdmin(setCriteria, setQuery, function (err, data) {
            cb(err,data);
        });
    }, function (cb) {
        if (tokenData && tokenData.id) {
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    tokenToSend = output && output.accessToken || null;
                    cb();
                }
            });

        } else {
            cb()
        }

    }], function (err, data) {
        console.log('sending response')
        responseToSend = {access_token: tokenToSend, ipAddress: userData.ipAddress};
        if (err) {
            callback(err);
        } else {
            callback(null,responseToSend)
        }

    });


};

var adminLogout = function (token, callback) {
    TokenManager.expireToken(token, function (err, data) {
        if (!err && data == 1) {
            callback(null, UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT);
        } else {
            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED)
        }
    })
};

var changePassword = function (queryData, userData, callback) {
    var userFound = null;
    if (!queryData.oldPassword || !queryData.newPassword || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var projection = {};
                var options = {
                    lean: true
                };
                Service.CustomerService.getCustomer(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        if (data && data.length > 0 && data[0]._id) {
                            userFound = data[0];
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                        }
                    }
                })
            },
            function (cb) {
                //Check Old Password
                if (userFound.password != UniversalFunctions.CryptData(queryData.oldPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASS)
                } else if (userFound.password == UniversalFunctions.CryptData(queryData.newPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.SAME_PASSWORD)
                } else {
                    cb();
                }
            },
            function (cb) {
                // Update User Here
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    $set: {
                        firstTimeLogin: false,
                        password: UniversalFunctions.CryptData(queryData.newPassword)
                    }
                };
                var options = {
                    lean: true
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb);
            }

        ], function (err, result) {
            callback(err, null);
        })
    }
};

var getCustomer = function (payloadData, callback) {
    var adminType;
    var adminId;
    var customer;
    async.auto({
            getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType;
                    cb(null);
                }
            })
        },
      
        getCust: ['getAdminId',function(cb){
      var query={
        isBlocked:false
      };
      var dataToGet = {};
      Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
        if(err){
            cb(err)
        }
        else{
        customer = result;
        cb(null);
        }
      })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            callback(null,customer);
        }

// Service.CustomerService.getCustomer(criteria, { __v: 0}, options, function (err, data) {
//         callback(err, {count: data && data.length || 0, customersArray: data})
//     })

    })    
};

var getPartner = function (queryData, callback) {
    var criteria = {};
    if (queryData.partnerId) {
        criteria._id = queryData.partnerId;
    }
    if (queryData.phoneNo) {
        criteria.phoneNo = queryData.phoneNo;
    }
    if (queryData.email) {
        criteria.email = queryData.email;
    }
    if (queryData.hasOwnProperty('isBlocked')) {
        criteria.isBlocked = queryData.isBlocked;
    }
    var options = {
        limit: queryData.limit || 0,
        skip: queryData.skip || 0,
        sort: {registrationDate: -1}
    };
    Service.PartnerService.getPartner(criteria, { __v: 0}, options, function (err, data) {
        callback(err, {count: data && data.length || 0, partnersArray: data})
    })
};

var getInvitedUsers = function (data, callback) {
    var criteria = {};
    if (data.phoneNo) {
        criteria.phoneNo = data.phoneNo;
    }
    if (data.deviceToken) {
        criteria.deviceToken = data.deviceToken;
    }
    if (data.appVersion) {
        criteria.appVersion = data.appVersion;
    }
    if (data.deviceType) {
        criteria.deviceType = data.deviceType;
    }
    if (data.referralCode) {
        criteria.referralCode = data.referralCode;
    }
    var options = {
        limit: data.limit || 0
        , skip: data.skip || 0,
        sort : {rank: 1}
    };
    Service.InvitedUserService.getUser(criteria, {__v: 0}, options, function (err, data) {
        callback(err, {count: data.length || 0, invitedUsersArray: data})
    })
};

var deleteCustomer = function (phoneNo, callback) {
    var userId = null;
    if (!phoneNo) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    phoneNo: phoneNo
                };
                Service.CustomerService.getCustomer(criteria, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            userId = userData[0] && userData[0]._id;
                            cb()
                        }
                    }
                })
            },
            /*function (cb) {
             //TODO use it later when bookings API are completed
             //Delete Booking
             var criteria = {
             $or: [{driver: userId}, {customer: userId}]
             };
             Service.Booking.deleteBooking(criteria, cb)
             },*/
            function (cb) {
                //Finally Delete User
                var criteria = {
                    _id: userId
                };
                Service.CustomerService.deleteCustomer(criteria, function (err, data) {
                    cb(err, data);
                })
            }
        ], function (err, result) {
            callback(err, null)

        })
    }
};

// var getContactBusiness= function (payload, callback) {
//     Service.ContactFormService.getBusinessData({},{__v:0},{lean:true}, function (err, businessArray) {
//         if (err){
//             callback(err)
//         }else {
//             callback(null,{count:businessArray && businessArray.length || 0, businessArray : businessArray || []})
//         }
//     })};

// var getContactDriver = function (payload, callback) {
//     Service.ContactFormService.getDriverData({},{__v:0},{lean:true}, function (err, driverArray) {
//         if (err){
//             callback(err)
//         }else {
//             callback(null,{count:driverArray && driverArray.length || 0, driverArray : driverArray || []})
//         }
//     })
// };


var createWages = function(payloadData,callback){
        //console.log(createWages)
var adminId;
var adminType;
var wagesImage={};
    async.auto({
            getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType;
                    cb(null);
                }
            })
        },
         uploadImage:["getAdminId",function(cb){
                var date = new Date().getTime() + adminId;
                         UploadManager.uploadFileToS3WithThumbnail(payloadData.images, date,0, function(err,result){

                             if(err){
                                 console.log("here err",err);
                                 cb(err)
                             }
                             else{
                                 //console.log()
                                 wagesImage.original =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                                 wagesImage.thumbnail = Config.awsS3Config.s3BucketCredentials.s3URL+result.thumbnail;
                             cb(null);
                             }
                         })
         }],
        insertWages: ['uploadImage',function(cb){
            var date =new Date().getTime() + UniversalFunctions.generateRandomString();
            for(var i=0;i<payloadData.count;i++){
                (function(i){
                    var obj = {
                        name:payloadData.name,
                        images:wagesImage,
                        referralCode1:UniversalFunctions.generateRandomString(),
                        referralCode2:UniversalFunctions.generateRandomString(),
                        createdAt:date,
                        createdBy:adminId,
                        description:payloadData.description,
                        cost:payloadData.cost
                    };
                    Service.WagerService.createWages(obj,function(err,result){
                        if(err){
                            console.log(err);
                            cb(err);
                        }
                        else{
                            if(i==payloadData.count-1)
                            cb(null);
                        }
                    })
                }(i))
            }
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            callback(null,{})
        }
    })
};


var viewWages = function(payloadData,callback){
    var adminId;
    var adminType;
    var wages;
    async.auto({
            getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType;
                    cb(null);
                }
            })
        },
      
        getWages: ['getAdminId',function(cb){
      var query={
        is_deleted:false
      };
      var dataToGet = {};
      Service.WagerService.getWagesByadmin(query,dataToGet,{lean:true},function(err,result){
        if(err){
            cb(err)
        }
        else{
        wages = result;
        cb(null);
        }
      })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            callback(null,wages)
        }
    })
};


var getIdByToken  = function(token,callback){
    var query = {
        accessToken:token
    };
    var options = {lean:true};
    var projections = {_id:1,adminType:1};
    Service.AdminService.getAdmin(query,projections,options,function(err,result){
        if(err){
            console.log(err);
            callback(err);
        }else{
            if(result.length){
               // console.log(result);
                callback(null,result)
            }else{
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED)
            }
        }
    })
}


var redeemedWages = function(payloadData,callback){
    var adminId;
    var adminType;
    var wages=0;
    console.log(payloadData);
    async.auto({
            getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType;
                    cb(null);
                }
            })
        },
      
      redeemed: ['getAdminId',function(cb){
      var query={
        _id:payloadData.wageId,
        is_deleted:false
      };
      var dataToSet = {
            redeemed:true,
            redeemedBy:adminId,
            redeemedAt: new Date()
      };
      Service.WagerService.updateWages(query,dataToSet,{lean:true},function(err,result){
        if(err){
            cb(err)
        }
        else{
        if(result){
                wages=1
        }
        cb(null);
        }
      })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            if(wages==1){
                callback(null,"redeemed")
            }
            else{
            callback(null,"Sorry This coupon has already been redeemed or deleted");
            }
            
        }
    })
}


var deleteWages = function(payloadData,callback){
    var adminId;
    var adminType;
    var wagesUpdated=0;
    async.auto({
            getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType
                    cb(null);
                }
            })
        },
      
      redeemed: ['getAdminId',function(cb){
var query;
        if(adminType==0){
            query={
        _id:payloadData.wageId,
      };
        }
        else{
        query ={
        _id:payloadData.wageId,
        createdBy:adminId
      };
        }
      
      var dataToSet = {
            is_deleted:true
      }
      Service.WagerService.updateWages(query,dataToSet,{lean:true},function(err,result){
        if(err){
            cb(err)
        }
        else{
            if(result){
                wagesUpdated=1
            }
            else{
                wagesUpdated=0
            }
        cb(null);
        }
      })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            if(wagesUpdated==0){
            callback(null,"sorry You dont have authority to delete this wager")

            }
            else{
            callback(null,"deleteWages")

            }
        }
    })
};

var createMerchant = function(payloadData,callback){

    var adminType;
    var adminId;
    var generatedPassword;
    var password;
    var id;
    var date;
    async.auto({
            getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType
                    cb(null);
                }
            })
        },
        password:['getAdminId',function (cb) {
            generatedPassword = UniversalFunctions.generateRandomString();
            password = UniversalFunctions.CryptData(generatedPassword);
            console.log(generatedPassword);
            cb(null);
        }],
      
      createNewMerchant: ['password',function(cb){
            var query =
            {
        address:payloadData.address,
        name:payloadData.name,
        phoneNo:payloadData.phoneNo,
        email:payloadData.email,
        password:password
      };
      Service.AdminService.createAdmin(query,function(err,result){
        if(err){
                    console.log(err);

            cb(err)
        }
        else{
            id = result._id;

        cb(null);
        }
      })
        }],
        sendMail:['createNewMerchant',function(cb){

            var subject = "Winmo";
            var content = "We welcome you to Winmo App<br>";
            content += "Email : " + payloadData.email + " \n <br>";
            content += "Password : " + generatedPassword + " \n<br>";
            content += "Thank You <br>\n";
            content += "\n\n<br>";
            content += "Team Winmo \n";
            emailPassword.sendEmail(payloadData.email,subject,content,cb);

        }],
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            var data = {
                    id:id,
                    date:new Date()
            }
            callback(null,data);
            }
        })
}



var deleteMerchent = function(payloadData,callback){
    var adminId;
    var adminType;
    var merchentDeleted=0;
    console.log(payloadData)
    async.auto({
            getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType
                   // console.log(adminType)
                    cb(null);
                }
            })
        },
      
      redeemed: ['getAdminId',function(cb){
        
      if(adminType==0){
        var query={
        _id:payloadData.merchentId,
        adminType:1
      };
       
      
      var dataToSet = {
            is_deleted:true
      };
      Service.AdminService.updateAdmin(query,dataToSet,{lean:true},function(err,result){
        if(err){
            cb(err)
        }
        else{
            if(result){
                merchentDeleted=1
            }
            else{
                merchentDeleted=0
            }
        cb(null);
        }
      })
      }
      else{
        cb(null);
      }
          
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            if(merchentDeleted==0){
            callback(null,"sorry You dont have authority to delete this merchent")

            }
            else{
            callback(null,"delete merchent")

            }
        }
    })
}

var getAllMerchents = function(payloadData,callback){
    var adminId;
    var adminType;
    var merchantList;
    async.auto({
            getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType;
                    console.log(adminType);
                    cb(null);
                }
            })
        },
      
        getMerchents: ['getAdminId',function(cb){
      var query={
        is_deleted:false,
        adminType:1
      };
      var dataToGet = {password:0};
      Service.AdminService.getAdmin(query,dataToGet,{lean:true},function(err,result){
        if(err){
            cb(err)
        }
        else{
        merchantList = result;
        cb(null);
        }
      })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            callback(null,merchantList)
        }
    })
};


var imageGames = function(payloadData,callback){

    var adminId;
    var adminType;
    var merchantList;

    console.log("============================g==========================",typeof payloadData.option2)
    async.auto({
        getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType;
                    cb(null);
                }
            })
        },
        uploadImage1:["getAdminId",function(cb){
            var date = new Date().getTime();
                    var id =adminId + date + "uploadImage1";
            if(payloadData.allImage !=0){
                UploadManager.uploadFileToS3WithThumbnail(payloadData.option1, id,0, function(err,result){

                    if(err){
                        console.log("here err",err);
                        cb(err)
                    }
                    else
                    {
                        payloadData.option1 =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        cb(null)
                    }
                })
            }
            else{
                cb(null)
            }

        }],
        uploadImage2:["getAdminId",function(cb){
            var date = new Date().getTime();
            var id =adminId + date + "uploadImage2";
            if(payloadData.allImage !=0){
                UploadManager.uploadFileToS3WithThumbnail(payloadData.option2, id,0, function(err,result){

                    if(err){
                        console.log("here err",err);
                        cb(err)
                    }
                    else
                    {
                        payloadData.option2 =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        cb(null)
                    }
                })
            }
            else{
                cb(null)
            }
        }],
        uploadImage3:["getAdminId",function(cb){
            var date = new Date().getTime();
            var id =adminId + date + "uploadImage3";
            if(payloadData.allImage !=0){
                UploadManager.uploadFileToS3WithThumbnail(payloadData.option3, id,0, function(err,result){

                    if(err){
                        console.log("here err",err);
                        cb(err)
                    }
                    else
                    {
                        payloadData.option3 =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        cb(null)
                    }
                })
            }
            else{
                cb(null)
            }
        }],
        uploadImage4:["getAdminId",function(cb){
            var date = new Date().getTime();
            var id =adminId + date + "uploadImage4";
            if(payloadData.allImage !=0){
                UploadManager.uploadFileToS3WithThumbnail(payloadData.option4, id,0, function(err,result){

                    if(err){
                        console.log("here err",err);
                        cb(err)
                    }
                    else
                    {
                        payloadData.option4 =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        cb(null)
                    }
                })
            }
            else{
                cb(null)
            }
        }],
        uploadImage5:["getAdminId",function(cb){
            var date = new Date().getTime();
            var id =adminId + date + "uploadImage5";
                UploadManager.uploadFileToS3WithThumbnail(payloadData.question, id,0, function(err,result){
                    if(err){
                        console.log("here err",err);
                        cb(err)
                    }
                    else
                    {
                        payloadData.question =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        cb(null)
                    }
                })
        }],

        updateQuestions: ['uploadImage5','uploadImage4','uploadImage3','uploadImage2','uploadImage1',function(cb){
            var questionText;
            if(payloadData.questionText != undefined){
                questionText=payloadData.questionText
            }
            else{
                questionText = null;
            }
            var dataToSet = {

                    "question":payloadData.question,
                    questionText:questionText,
                    "optionImage1": payloadData.option1,
                    "optionImage2": payloadData.option2,
                    "optionImage3": payloadData.option3,
                    "optionImage4": payloadData.option4,
                    "answer":payloadData.correctAnswer,
                  //  "gameSubType":payloadData.gameSubType,
                    "gameId":payloadData.gameId
            };

            Service.ImageGameService.createImageService(dataToSet,function(err,result){
                if(err){
                    cb(err)
                }
                else{
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            callback(null,{});
        }
    })
};



var getImageGames =function(payloadData,callback){

    var adminId;
    var adminType;
    var imageGame;
    async.auto({
        getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType;
                  //  console.log(adminType)
                    cb(null);
                }
            })
        },

        getImageData: ['getAdminId',function(cb){
            var query={
                gameId:payloadData.gameId,
                isDeleted:false
            };
            Service.ImageGameService.getImageService(query,{},{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                    imageGame = result;
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            callback(null,imageGame);
        }
    })
};





var deleteImageGames =function(payloadData,callback){

    var adminId;
    var adminType;
    var imageGame;
    async.auto({
        getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType;
                    //  console.log(adminType)
                    cb(null);
                }
            })
        },

        getImageData: ['getAdminId',function(cb){
            var query={
                _id:payloadData.imageGameId,
            };

            Service.ImageGameService.updateImageService(query,{isDeleted:true},{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                  //  imageGame = result;
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            callback(null,imageGame);
        }
    })
};

var viewUserGames = function (payloadData, callback) {
    var adminType;
    var adminId;
    var customer;
    async.auto({
        getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    adminId = result[0]._id;
                    adminType = result[0].adminType;
                    cb(null);
                }
            })
        },

        getCustGames: ['getAdminId',function(cb){
            var query={
                $or:[{sender:payloadData.userId,
                    receiver:payloadData.userId}]
            };
            var dataToGet = {

            };
            Service.RequestGamesService.getRequestGames(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(err)
                }
                else{
                    customer = result;
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            callback(null,customer);
        }
    })
};


module.exports = {
    deleteCustomer: deleteCustomer,
    deleteDriver: changePassword,
    //getContactDriver : getContactDriver,
  //  getContactBusiness : getContactBusiness,
    adminLogin: adminLogin,
    adminLogout: adminLogout,
    updateCustomer: updateCustomer,
    getCustomer: getCustomer,
    getInvitedUsers: getInvitedUsers,
    getPartner: getPartner,
    createWages:createWages,
    viewWages:viewWages,
    redeemedWages:redeemedWages,
    deleteWages:deleteWages,
    createMerchant:createMerchant,
    getAllMerchents:getAllMerchents,
    deleteMerchent:deleteMerchent,
    imageGames:imageGames,
    getImageGames:getImageGames,
    deleteImageGames:deleteImageGames,
    viewUserGames:viewUserGames
};