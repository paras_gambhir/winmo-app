'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var CodeGenerator = require('../Lib/CodeGenerator');

function clearDeviceTokenFromDB(token,cb){
    if (token) {
        var criteria = {
            deviceToken: token
        };
        var setQuery = {
            $unset: {deviceToken: 1}
        };
        var options = {
            multi: true
        };
        Service.DriverService.updateDriver(criteria, setQuery, options, cb)
    } else {
        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }
}

var createDriver = function (payloadData, callback) {
    console.log('sending',payloadData);
    payloadData.facebookId = UniversalFunctions.sanitizeName(payloadData.facebookId);
    var accessToken = null;
    var uniqueCode = null;
    var dataToSave = payloadData;
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    dataToSave.firstTimeLogin = false;
    var driverData = null;
    var dataToUpdate = {};
    if (payloadData.profilePic && payloadData.profilePic.filename) {
        dataToUpdate.profilePicURL = {
            original: null,
            thumbnail: null
        }
    }

    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate for facebookId and password
            console.log('datadataToSave.facebookId',dataToSave.facebookId)
            if (~~(dataToSave.facebookId)&&dataToSave.facebookId && dataToSave.facebookId.length > 0) {
                console.log('datadataToSave.facebookId',dataToSave.facebookId)

                if (dataToSave.password) {
                    console.log('datadataToSave.facebookId',dataToSave.facebookId)

                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.FACEBOOK_ID_PASSWORD_ERROR);
                } else {
                    cb();
                }
            } else if (!dataToSave.password) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PASSWORD_REQUIRED);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate countryCode
            if (dataToSave.countryCode.lastIndexOf('+') == 0) {
                if (!isFinite(dataToSave.countryCode.substr(1))) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
                else {
                    cb();
                }
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
            }
        },
        function (cb) {
            //Validate phone No
            console.log('dataToSave.phoneNo',dataToSave.phoneNo)
            if (dataToSave.phoneNo && dataToSave.phoneNo.split('')[0] == 0) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        function (cb) {
            CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            })
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            var criteria = {
                deviceToken: dataToSave.deviceToken
            };
            var setQuery = {
                $unset: {deviceToken: 1}
            };
            var options = {
                multi: true
            };
            Service.DriverService.updateDriver(criteria, setQuery, options, cb)
        },
        function (cb) {
            //Insert Into DB
            dataToSave.OTPCode = uniqueCode;
            dataToSave.registrationDate = new Date().toISOString();
            dataToSave.emailVerificationToken = UniversalFunctions.CryptData(JSON.stringify(dataToSave));
            Service.DriverService.createDriver(dataToSave, function (err, dataFromDB) {
                console.log('customeized erro',err,dataFromDB)
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('drivers.$phoneNo_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('drivers.$email_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

                    }else {
                        cb(err)
                    }
                } else {
                    console.log('setting driver',driverData)
                    driverData = dataFromDB;
                    console.log('setting driver',driverData)
                    cb();
                }
            })
        },
        function (cb) {
            //Check if profile pic is being updated
            if (driverData && driverData._id && payloadData.profilePic && payloadData.profilePic.filename) {
                UploadManager.uploadFileToS3WithThumbnail(payloadData.profilePic, driverData._id, function (err, uploadedInfo) {
                    console.log('update profile pic',err,uploadedInfo)
                    if (err) {
                        cb(err)
                    } else {
                        dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                        dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
                        cb();
                    }
                })
            } else {
                cb();
            }
        },
        function (cb) {
            if (driverData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
                //Update User
                var criteria = {
                    _id: driverData._id
                };
                var setQuery = {
                    $set: dataToUpdate
                };
                Service.DriverService.updateDriver(criteria, setQuery, {new: true}, function (err, updatedData) {
                    driverData = updatedData;
                    cb(err, updatedData)
                })
            }else {
                if (driverData && driverData._id && payloadData.profilePic && payloadData.profilePic.filename && !dataToUpdate.profilePicURL.original){
                    var criteria = {
                        _id: driverData._id
                    };
                    Service.DriverService.deleteDriver(criteria,function (err, updatedData) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ERROR_PROFILE_PIC_UPLOAD);
                    })
                }else {
                    cb();
                }
            }
        },
        function (cb) {
            //clearDeviceTokenFromDB
            console.log('payloadData.deviceToken && driverData',payloadData.deviceToken , driverData)
            if (payloadData.deviceToken && driverData) {
                clearDeviceTokenFromDB(payloadData.deviceToken, cb)
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        },
        function (cb) {
            //Send SMS to User
            if (driverData) {
                NotificationManager.sendSMSToUser(uniqueCode, dataToSave.countryCode, dataToSave.phoneNo, function (err, data) {
                    cb();
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }

        },
        function (cb) {
            //Send Verification Email
            if (driverData) {
                var emailType = 'REGISTRATION_MAIL';
                var variableDetails = {
                    user_name: dataToSave.name,
                    verification_url: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.DOMAIN_NAME + '/api/driver/verifyEmail/' + dataToSave.emailVerificationToken
                };
                var emailAddress = dataToSave.email;
                NotificationManager.sendEmailToUser(emailType, variableDetails, emailAddress, cb)
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }

        },
        function (cb) {
            //Set Access Token
            if (driverData) {
                var tokenData = {
                    id: driverData._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(driverData.toObject())
            });
        }
    });
};

var loginDriver = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var flushPreviousSessions = payloadData.flushPreviousSessions || false;
    var updatedUserDetails = null;
    async.series([
        function (cb) {
            var criteria = {
                email: payloadData.email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    cb();
                }
            });

        },
        function (cb) {
            //validations
            if (!userFound) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
            } else {
                if (userFound && userFound.password != UniversalFunctions.CryptData(payloadData.password)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                } else {
                    successLogin = true;
                    cb();
                }
            }
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            if (userFound && payloadData.deviceToken != userFound.deviceToken && !flushPreviousSessions){
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACTIVE_PREVIOUS_SESSIONS)
            } else {
                var criteria = {
                    deviceToken: payloadData.deviceToken
                };
                var setQuery = {
                    $unset: {deviceToken: 1}
                };
                var options = {
                    multi: true
                };
                Service.DriverService.updateDriver(criteria, setQuery, options, cb)
            }
        },
        function (cb) {
            var criteria = {
                _id: userFound._id
            };
            var setQuery = {
                appVersion: payloadData.appVersion,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType
            };
            Service.DriverService.updateDriver(criteria, setQuery, {new:true}, function (err, data) {
                updatedUserDetails = data;
                cb(err, data);
            });

        },
        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken){
                            accessToken = output && output.accessToken;
                            cb();
                        }else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
            }

        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {accessToken: accessToken, userDetails: UniversalFunctions.deleteUnnecessaryUserData(updatedUserDetails.toObject())});
        }
    });
};

var logoutDriver = function (userData, callback) {
    if (!userData || !userData.id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userId = userData && userData.id || 1;

        async.series([
            function (cb) {
                //Check if the driver is free or not
                Service.DriverService.getDriver({_id : userData.id}, {availabilityStatus : 1},{lean:true}, function (err, driverAry) {
                    if (err){
                        cb(err)
                    }else if (driverAry && driverAry[0]){
                        if (driverAry[0].availabilityStatus){
                            cb()
                        }else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.CANNOT_LOGOUT)
                        }
                    }else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                    }
                })
            },
            function (cb) {
                var criteria = {
                    _id: userId
                };
                var setQuery = {
                    $unset: {
                        accessToken: 1
                    }
                };
                var options = {};
                Service.DriverService.updateDriver(criteria, setQuery, options, cb);
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};

var updateDriver = function (userPayload,userData, callback) {
    var dataToUpdate = {};
    var updatedUser = null;
    if (!userPayload || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var samePhoneNo = (userData.phoneNo == userPayload.phoneNo) || false;
        if (userPayload.name && userPayload.name!='') {
            dataToUpdate.name = UniversalFunctions.sanitizeName(userPayload.name);
        }
        if (userPayload.deviceToken && userPayload.deviceToken!='') {
            dataToUpdate.deviceToken = userPayload.deviceToken;
        }
        if (userPayload.phoneNo && userPayload.phoneNo!='') {
            dataToUpdate.phoneNo = userPayload.phoneNo;
        }
        if (userPayload.profilePic && userPayload.profilePic.filename) {
            dataToUpdate.profilePicURL = {
                original: null,
                thumbnail: null
            }
        }
        if (Object.keys(dataToUpdate).length == 0){
            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOTHING_TO_UPDATE);
        }else {
            async.series([
                function (cb) {
                    //Check all empty values validations
                    if (userPayload.name && !dataToUpdate.name){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMPTY_VALUE)
                    }else if (dataToUpdate.name){
                        UniversalFunctions.customQueryDataValidations('NAME','name',dataToUpdate.name, cb)
                    }else {
                        cb();
                    }

                },
                function (cb) {
                    //Check if profile pic is being updated
                    if (userPayload.profilePic && userPayload.profilePic.filename) {
                        UploadManager.uploadFileToS3WithThumbnail(userPayload.profilePic, userData.id, function (err, uploadedInfo) {
                            console.log('err in profile pic',err,uploadedInfo)
                            if (err) {
                                cb(err)
                            } else {
                                dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                                dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
                                cb();
                            }
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (userPayload.phoneNo && !samePhoneNo) {
                        var criteria = {
                            phoneNo: userPayload.phoneNo
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                            if (err) {
                                cb(err)
                            } else {
                                if (result && result.length > 0) {
                                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST)
                                } else {
                                    cb();
                                }
                            }
                        });
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Update User
                    var criteria = {
                        _id: userData.id
                    };
                    var setQuery = {
                        $set: dataToUpdate
                    };
                    Service.CustomerService.updateCustomer(criteria, setQuery, {new:true}, function (err, updatedData) {
                        updatedUser = updatedData;
                        cb(err,updatedData)
                    })
                }
            ], function (err, result) {
                callback(err, {userData : UniversalFunctions.deleteUnnecessaryUserData(updatedUser.toObject())});
            })
        }
    }
};

var resetPassword = function (email, callback) {
    var generatedPassword = UniversalFunctions.generateRandomString();
    var driverObj = null;
    if (!email) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    email: email
                };
                var setQuery = {
                    firstTimeLogin : true,
                    password: UniversalFunctions.CryptData(generatedPassword)
                };
                Service.DriverService.updateDriver(criteria, setQuery, {new: true}, function (err, userData) {
                    console.log('update driver',err,userData)
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            driverObj = userData;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                if (driverObj) {
                    var variableDetails = {
                        user_name: driverObj.name,
                        password_to_login: generatedPassword
                    };
                    NotificationManager.sendEmailToUser(variableDetails, driverObj.email, cb)
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            callback(err, {generatedPassword: generatedPassword}); //TODO Change in production DO NOT Expose the password
        })
    }
};

var changePassword = function (queryData,userData, callback) {
    var userFound = null;
    if (!queryData.oldPassword || !queryData.newPassword || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id : userData.id
                };
                var projection = {};
                var options = {
                    lean: true
                };
                Service.DriverService.getDriver(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        if (data && data.length > 0 && data[0]._id) {
                            userFound = data[0];
                            cb();
                        }else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                        }
                    }
                })
            },
            function (cb) {
                //Check Old Password
                if (userFound.password != UniversalFunctions.CryptData(queryData.oldPassword)){
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASS)
                }else if (userFound.password == UniversalFunctions.CryptData(queryData.newPassword)){
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.SAME_PASSWORD)
                }else {
                    cb();
                }
            },
            function (cb) {
                // Update User Here
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    $set: {
                        firstTimeLogin : false,
                        password: UniversalFunctions.CryptData(queryData.newPassword)
                    }
                };
                var options = {
                    lean: true
                };
                Service.DriverService.updateDriver(criteria, setQuery, options, cb);
            }

        ], function (err, result) {
            callback(err, null);
        })
    }
};

var verifyEmail = function (emailVerificationToken, callback) {
    if (!emailVerificationToken){
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }else {
        var userData = null;
        async.series([
            function (cb) {
                var criteria = {
                    emailVerificationToken : emailVerificationToken
                };
                var setQuery  = {
                    $set : {emailVerified: true},
                    $unset : {emailVerificationToken : 1}
                };
                var options = {new:true};
                Service.DriverService.updateDriver(criteria,setQuery,options, function (err, updatedData) {
                    if (err){
                        cb(err)
                    }else {
                        if (!updatedData){
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                        }else {
                            userData = updatedData;
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err){
                callback(err)
            }else {
                callback(null,UniversalFunctions.deleteUnnecessaryUserData(userData.toObject()));
            }

        });
    }

};

var verifyOTP = function (OTP, userData, callback) {
    if (!OTP || !userData || !userData.id){
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }else {
        async.series([
            function (cb) {
                //Check verification code :
                if (OTP == userData.OTPCode){
                    cb();
                }else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                }
            },
            function (cb) {
                var criteria = {
                    _id : userData.id,
                    OTPCode : OTP
                };
                var setQuery  = {
                    $set : {phoneVerified : true},
                    $unset : {OTPCode : 1}
                };
                if (!userData.phoneVerified && userData.newNumber){
                    setQuery.$set.phoneNo = userData.newNumber;
                    setQuery.$unset.newNumber = 1;
                }
                var options = {new:true};
                Service.DriverService.updateDriver(criteria,setQuery,options, function (err, updatedData) {
                    if (err){
                        cb(err)
                    }else {
                        if (!updatedData){
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                        }else {
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err){
                callback(err)
            }else {
                callback();
            }

        });
    }

};

var updateBankAccountDetails = function (userPayload, userData, callback) {
    if (!userData || !userData.id){
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }else {
            var criteria = {
                _id : userData.id
            };
            var setQuery  = {
                $set : {
                    paymentDetails : userPayload,
                    registrationCompleted : true
                }
            };
            var options = {new:true};
            Service.DriverService.updateDriver(criteria,setQuery,options, function (err, updatedData) {
                if (err){
                    callback(err)
                }else {
                    if (!updatedData){
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                    }else {
                        callback(null,{userDetails : updatedData});
                    }
                }
            });
    }
};

var resendOTP = function (userData, callback) {
    /*
     Create a Unique 4 digit code
     Insert It Into  DB
     Send the 4 digit code via SMS
     Send Back Response
     */
    var phoneNo = userData.newNumber || userData.phoneNo;
    var countryCode = userData.countryCode;
    if (!phoneNo) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var uniqueCode = null;
        async.series([
            function (cb) {
                CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER,function (err, numberObj) {
                    if (err) {
                        cb(err);
                    } else {
                        if (!numberObj || numberObj.number == null) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                        } else {
                            uniqueCode = numberObj.number;
                            cb();
                        }
                    }
                })
            },
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var setQuery = {
                    $set: {
                        OTPCode: uniqueCode,
                        codeUpdatedAt: new Date().toISOString()
                    }
                };
                var options = {
                    lean: true
                };
                Service.DriverService.updateDriver(criteria, setQuery, options, cb);
            }, function (cb) {
                //Send SMS to User
                NotificationManager.sendSMSToUser(uniqueCode, countryCode, phoneNo, function (err, data) {
                    cb();
                })
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};

var resendEmailVerificationToken = function (email, callback) {
    if (!email) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userFound = null;
        var emailVerificationToken = null;
        async.series([
            function (cb) {
                //verify email address
                if (!UniversalFunctions.verifyEmailFormat(email)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                } else {
                    cb();
                }
            },
            function (cb) {
                var criteria = {
                    email: email
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.DriverService.getDriver(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = result && result[0] || null;
                        cb();
                    }
                });

            },
            function (cb) {
                //validations
                if (!userFound) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
                } else {
                    emailVerificationToken = UniversalFunctions.CryptData(JSON.stringify(new Date().toISOString()));
                    cb();
                }
            },
            function (cb) {
                //Update emailverification token in DB
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    emailVerificationToken : emailVerificationToken
                };
                var option = {
                    new: true
                };
                Service.DriverService.updateDriver(criteria, setQuery, option, function (err, updatedData) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = updatedData;
                        cb();
                    }
                });            },
            function (cb) {
                //Send Verification Email
                if (userFound) {
                    var emailType = 'REGISTRATION_MAIL';
                    var variableDetails = {
                        user_name: userFound.name,
                        verification_url: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.DOMAIN_NAME + '/api/driver/verifyEmail/' + emailVerificationToken
                    };
                    NotificationManager.sendEmailToUser(emailType, variableDetails, email, cb)
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }

            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};

module.exports = {
    createDriver: createDriver,
    verifyEmail: verifyEmail,
    verifyOTP: verifyOTP,
    resendOTP: resendOTP,
    loginDriver: loginDriver,
    logoutDriver: logoutDriver,
    updateBankAccountDetails: updateBankAccountDetails,
    resendEmailVerificationToken: resendEmailVerificationToken,
    changePassword: changePassword,
    resetPassword: resetPassword,
    updateDriver: updateDriver
};