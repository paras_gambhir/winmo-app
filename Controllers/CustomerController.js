'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var SocketManager = require('../Lib/SocketManager');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var CodeGenerator = require('../Lib/CodeGenerator');
var Config = require('../Config');

var stripe = require("stripe")(
    Config.APP_CONSTANTS.SERVER.STRIPE_KEY
);

function clearDeviceTokenFromDB(token, cb) {
    if (token) {
        var criteria = {
            deviceToken: token
        };
        var setQuery = {
            $unset: {deviceToken: 1}
        };
        var options = {
            multi: true
        };
        Service.CustomerService.updateCustomer(criteria, setQuery, options, cb)
    } else {
        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }
}

var createCustomer = function (payloadData, callback) {
    console.log('sending',payloadData)
    var accessToken = null;
    var uniqueCode = null;
    var dataToSave = payloadData;
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    dataToSave.firstTimeLogin = false;
    var customerData = null;
    var dataToUpdate = {};
    if (payloadData.profilePic && payloadData.profilePic.filename) {
        dataToUpdate.profilePicURL = {
            original: null,
            thumbnail: null
        }
    }

    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate for facebookId and password
            if (dataToSave.facebookId) {
                if (dataToSave.password) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.FACEBOOK_ID_PASSWORD_ERROR);
                } else {
                    cb();
                }
            } else if (!dataToSave.password) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PASSWORD_REQUIRED);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate countryCode
            if (dataToSave.countryCode.lastIndexOf('+') == 0) {
                if (!isFinite(dataToSave.countryCode.substr(1))) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
                else {
                    cb();
                }
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
            }
        },
        function (cb) {
            //Validate phone No
            if (dataToSave.phoneNo && dataToSave.phoneNo.split('')[0] == 0) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        function (cb) {
            CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            })
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            var criteria = {
                deviceToken: dataToSave.deviceToken
            };
            var setQuery = {
                $unset: {deviceToken: 1}
            };
            var options = {
                multi: true
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, options, cb)
        },
        function (cb) {
            //Insert Into DB
            dataToSave.OTPCode = uniqueCode;
            dataToSave.newNumber = payloadData.phoneNo;
            dataToSave.registrationDate = new Date().toISOString();
            dataToSave.emailVerificationToken = UniversalFunctions.CryptData(JSON.stringify(dataToSave));
            Service.CustomerService.createCustomer(dataToSave, function (err, customerDataFromDB) {
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('customers.$phoneNo_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

                    }else {
                        cb(err)
                    }
                } else {
                    customerData = customerDataFromDB;
                    cb();
                }
            })
        },
        function (cb) {
            //Check if profile pic is being updated
            if (customerData && customerData._id && payloadData.profilePic && payloadData.profilePic.filename) {
                UploadManager.uploadFileToS3WithThumbnail(payloadData.profilePic, customerData._id,0, function (err, uploadedInfo) {
                    console.log('update profile pic',err,uploadedInfo)
                    if (err) {
                        cb(err)
                    } else {
                        dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                        dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
                        cb();
                    }
                })
            } else {
                cb();
            }
        },
        function (cb) {
            if (customerData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
                //Update User
                var criteria = {
                    _id: customerData._id
                };
                var setQuery = {
                    $set: dataToUpdate
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, updatedData) {
                    customerData = updatedData;
                    cb(err, updatedData)
                })
            }else {
                if (customerData && customerData._id && payloadData.profilePic && payloadData.profilePic.filename && !dataToUpdate.profilePicURL.original){
                    var criteria = {
                        _id: customerData._id
                    };
                    Service.CustomerService.deleteCustomer(criteria,function (err, updatedData) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ERROR_PROFILE_PIC_UPLOAD);
                    })
                }else {
                    cb();
                }
            }
        },
        function (cb) {
            //clearDeviceTokenFromDB
            if (dataToSave.deviceToken && customerData) {
                clearDeviceTokenFromDB(dataToSave.deviceToken, cb)
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        },
        function (cb) {
            //Send SMS to User
            if (customerData) {
                NotificationManager.sendSMSToUser(uniqueCode, dataToSave.countryCode, dataToSave.phoneNo, function (err, data) {
                    cb();
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }

        },
        function (cb) {
            //Send Verification Email
            if (customerData) {
                var emailType = 'REGISTRATION_MAIL';
                var variableDetails = {
                    user_name: dataToSave.name,
                    verification_url: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.DOMAIN_NAME + '/api/customer/verifyEmail/' + dataToSave.emailVerificationToken
                };
                var emailAddress = dataToSave.email;
                NotificationManager.sendEmailToUser(emailType, variableDetails, emailAddress, cb)
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }

        },
        function (cb) {
            //Set Access Token
            if (customerData) {
                var tokenData = {
                    id: customerData._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(customerData.toObject())
            });
        }
    });
};

var loginCustomer = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var flushPreviousSessions = payloadData.flushPreviousSessions || false;
    var updatedUserDetails = null;
    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(payloadData.email)) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            var criteria = {
                email: payloadData.email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    cb();
                }
            });

        },
        function (cb) {
            //validations
            if (!userFound) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
            } else {
                if (userFound && userFound.password != UniversalFunctions.CryptData(payloadData.password)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                } else {
                    successLogin = true;
                    cb();
                }
            }
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            if (userFound && payloadData.deviceToken != userFound.deviceToken && !flushPreviousSessions) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACTIVE_PREVIOUS_SESSIONS)
            } else {
                var criteria = {
                    deviceToken: payloadData.deviceToken
                };
                var setQuery = {
                    $unset: {deviceToken: 1}
                };
                var options = {
                    multi: true
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb)
            }
        },
        function (cb) {
            var criteria = {
                _id: userFound._id
            };
            var setQuery = {
                appVersion: payloadData.appVersion,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType,
                language: payloadData.language
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                updatedUserDetails = data;
                cb(err, data);
            });

        },
        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
            }

        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(updatedUserDetails.toObject())
            });
        }
    });
};

var loginCustomerViaFacebook = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var flushPreviousSessions = payloadData.flushPreviousSessions || false;
    var updatedUserDetails = null;
    var dataReturn;
    var id;

    async.series([
        function (cb) {
            var criteria = {
                facebookId: payloadData.facebookId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                  //  console.log("==========sasa================",result);
                    userFound = result && result[0] || null;
                    console.log(userFound);
                    cb();
                }
            });

        },
        function (cb) {
            if(userFound != null){
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    appVersion: payloadData.appVersion,
                    deviceToken: payloadData.deviceToken,
                    deviceType: payloadData.deviceType,
                    currentLocation :[payloadData.long,payloadData.lat]

                    //  accessToken:accessToken

                };
                Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                    updatedUserDetails = data;
                    console.log("=============================================",data);
                    id = data._id;
                    cb(err, data);
                });

            }else{
                var obj = {
                  //  accessToken:accessToken,
                    facebookId:payloadData.facebookId,
                    gender:payloadData.gender || '',
                    city:payloadData.city || '',
                    occupation:payloadData.occupation || '',
                    employment:payloadData.employment || '',
                    school:payloadData.school || '',
                    age:payloadData.age || 0,
                    appVersion: payloadData.appVersion,
                    deviceToken: payloadData.deviceToken,
                    deviceType: payloadData.deviceType,
                    name:payloadData.name,
                    facts:payloadData.facts,
                    currentLocation :[payloadData.long,payloadData.lat]

                };
                Service.CustomerService.createCustomer(obj,function(err,result){
                    if(err){
                       // console.log(err)
                        cb(err);
                    }else{
                            id = result._id;
                        cb(null);
                    }
                })

            }
        },
       
        function(cb){
            var tokenData = {
                id: id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })
        },

        function(cb){
            var query = {
                accessToken:accessToken,
            }
            var projections = {
            }
            var options = {lean:true};
            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    if(result.length){
                        console.log(result);
                        dataReturn = result;

                      //  console.log(dataReturn);
                        cb(null)
                    }else{
                        cb(null)
                    }
                }
            })
        }
    ], function (err, data) {
        if (err) {
           // console.log(err)
            callback(err);
        } else {
            callback(null,dataReturn);
        }
    });
};

/*var updateCustomer = function (userPayload, userData, callback) {
    var dataToUpdate = {};
    var updatedUser = null;
    var samePhoneNo = false;
    var uniqueCode = null;
    var phoneNoUpdateRequest = false;
    var newCountryCode = null;
    if (!userPayload || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        if (userPayload.phoneNo && userPayload.countryCode) {
            samePhoneNo = (userData.phoneNo == userPayload.phoneNo);
            samePhoneNo = (userData.countryCode == userPayload.phoneNo);
        }
        if (userPayload.name && userPayload.name != '') {
            dataToUpdate.name = UniversalFunctions.sanitizeName(userPayload.name);
        }
        if (userPayload.deviceToken && userPayload.deviceToken != '') {
            dataToUpdate.deviceToken = userPayload.deviceToken;
        }
        if (userPayload.phoneNo && userPayload.countryCode &&
            userData.phoneNo == userPayload.phoneNo && userData.countryCode == userPayload.countryCode) {
            delete userPayload.phoneNo;
            delete userPayload.countryCode;
        }

        if (userPayload.phoneNo && userPayload.phoneNo != '') {
            dataToUpdate.newNumber = userPayload.phoneNo;
        }
        if (userPayload.countryCode && userPayload.countryCode != '') {
            newCountryCode = userPayload.countryCode;
        }
        if (userPayload.email && userPayload.email != '') {
            dataToUpdate.email = userPayload.email;
        }
        if (userPayload.language && (userPayload.language == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.EN
            || userPayload.language == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX)) {
            dataToUpdate.language = userPayload.language;
        }
        if (userPayload.profilePic && userPayload.profilePic.filename) {
            dataToUpdate.profilePicURL = {
                original: null,
                thumbnail: null
            }
        }
        if (Object.keys(dataToUpdate).length == 0) {
            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOTHING_TO_UPDATE);
        } else {
            async.series([
                function (cb) {
                    //verify email address
                    if (dataToUpdate.email && !UniversalFunctions.verifyEmailFormat(dataToUpdate.email)) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Validate phone No
                    if (userPayload.phoneNo && userPayload.phoneNo.split('')[0] == 0) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Validate countryCode and phoneNo
                    if (userPayload.countryCode) {
                        if (userPayload.countryCode.lastIndexOf('+') == 0) {
                            if (!isFinite(userPayload.countryCode.substr(1))) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                            }
                            else if (!userPayload.phoneNo || userPayload.phoneNo == '') {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_MISSING);
                            } else {
                                cb();
                            }
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                        }
                    } else if (userPayload.phoneNo) {
                        if (!userPayload.countryCode) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COUNTRY_CODE_MISSING);
                        } else if (userPayload.countryCode && userPayload.countryCode.lastIndexOf('+') == 0) {
                            if (!isFinite(userPayload.countryCode.substr(1))) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                            } else {
                                cb();
                            }
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                        }
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Check all empty values validations
                    if (userPayload.name && !dataToUpdate.name) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMPTY_VALUE)
                    } else if (dataToUpdate.name) {
                        UniversalFunctions.customQueryDataValidations('NAME', 'name', dataToUpdate.name, cb)
                    } else {
                        cb();
                    }

                },
                function (cb) {
                    //Check if profile pic is being updated
                    if (userPayload.profilePic && userPayload.profilePic.filename) {
                        UploadManager.uploadFileToS3WithThumbnail(userPayload.profilePic, userData.id, function (err, uploadedInfo) {
                            if (err) {
                                cb(err)
                            } else {
                                dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                                dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
                                cb();
                            }
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (userPayload.countryCode && userPayload.phoneNo && !samePhoneNo) {
                        var criteria = {
                            countryCode: userPayload.countryCode,
                            phoneNo: userPayload.phoneNo
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                            if (err) {
                                cb(err)
                            } else {
                                if (result && result.length > 0) {
                                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST)
                                } else {
                                    cb();
                                }
                            }
                        });
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (!samePhoneNo && userPayload.phoneNo) {
                        CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                            if (err) {
                                cb(err);
                            } else {
                                if (!numberObj || numberObj.number == null) {
                                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                                } else {
                                    uniqueCode = numberObj.number;
                                    cb();
                                }
                            }
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (newCountryCode && dataToUpdate.newNumber && !samePhoneNo) {
                        //Send SMS to User on the new number
                        UniversalFunctions.NotificationManager.sendSMSToUser(uniqueCode, newCountryCode, dataToUpdate.newNumber, function (err, data) {
                            dataToUpdate.OTPCode = uniqueCode;
                            dataToUpdate.newNumber = newCountryCode + '-' + dataToUpdate.newNumber;
                            phoneNoUpdateRequest = true;
                            cb();
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Update User
                    var criteria = {
                        _id: userData._id
                    };
                    var setQuery = {
                        $set: dataToUpdate
                    };
                    Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, updatedData) {
                        updatedUser = updatedData;
                        cb(err, updatedData)
                    })
                }
            ], function (err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, {
                        phoneNoUpdateRequest: phoneNoUpdateRequest,
                        userData: UniversalFunctions.deleteUnnecessaryUserData(updatedUser.toObject())
                    });
                }
            })
        }
    }
};*/


var profileCompleted = function(payloadData, callback){
    var userId;
    var data;
    async.auto({
        getUserId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        updateData:['getUserId',function(cb){
            var query = {_id:userId};
            var options = {new:true};
            var setData = {
                age:payloadData.age,
                city:payloadData.city,
                occupation:payloadData.occupation,
                employment:payloadData.employment,
                school:payloadData.school,
                gender:payloadData.gender,
                profileComplete:true,
                facts:payloadData.facts
            };
            Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    data = result;
                    cb(null)
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data);
        }
    })
};





var uploadImage0 = function(payloadData, callback){
    var userId;
    var data;
    console.log(payloadData);
    async.auto({
        getUserId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        uploadImage:["getUserId",function(cb){
            if( payloadData.images){
                var profilePic={};
                var id = userId + "1";
                var query = {
                    "profilePicURL.original": payloadData.images
                };
                Service.ImagesService.getImages(query,{},{},function(err,result){
                    if(err){
                        cb(err)
                    }
                    else{
                        if(result.length){
                            cb(null);
                        }
                        else{
                            profilePic.original = payloadData.images;
                            profilePic.thumbnail = payloadData.images;
                            var data = {
                                profilePicURL:profilePic,
                                customer:userId
                            };
                           // console.log(profilePic);
                            Service.ImagesService.createImages(data,function(err,result){
                                if(err){
                                    console.log(err)
                                }
                                else{
                                    cb(null)

                                }
                            })
                        }
                    }
                })
            }
            else{
                cb(null)
            }

        }],
        uploadImage1:["getUserId",function(cb){
            if( payloadData.images1){
                var profilePic={};
                var id = userId + "2";
                UploadManager.uploadFileToS3WithThumbnail(payloadData.images1, id,0, function(err,result){

                    if(err){
                        console.log("here err",err);
                        cb()
                    }
                    else{
                        profilePic.original =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        profilePic.thumbnail = Config.awsS3Config.s3BucketCredentials.s3URL+result.thumbnail;
                        var query = {
                            "profilePicURL.original": profilePic.original
                        };
                        Service.ImagesService.getImages(query,{},{},function(err,result){
                            if(err){
                                cb(err)
                            }
                            else{
                                if(result.length){
                                    cb(null);
                                }
                                else{
                                    var data = {
                                        profilePicURL:profilePic,
                                        customer:userId
                                    };
                                    console.log(profilePic);
                                    Service.ImagesService.createImages(data,function(err,result){
                                        if(err){
                                            console.log(err)
                                        }
                                        else{
                                            cb(null)

                                        }
                                    })
                                }
                            }
                        });


                    }
                })


            }
            else{
                cb(null)
            }

        }],
        uploadImage2:["getUserId",function(cb){
            if( payloadData.images2){
                var profilePic={};
                var id = userId + "3";
                UploadManager.uploadFileToS3WithThumbnail(payloadData.images2, id,0, function(err,result){

                    if(err){
                        console.log("here err",err);
                        cb()
                    }
                    else{
                        profilePic.original =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        profilePic.thumbnail = Config.awsS3Config.s3BucketCredentials.s3URL+result.thumbnail;
                        var query = {
                            "profilePicURL.original": profilePic.original
                        };
                        Service.ImagesService.getImages(query,{},{},function(err,result){
                            if(err){
                                cb(err)
                            }
                            else{
                                if(result.length){
                                    cb(null);
                                }
                                else{
                                    var data = {
                                        profilePicURL:profilePic,
                                        customer:userId
                                    };
                                    console.log(profilePic);
                                    Service.ImagesService.createImages(data,function(err,result){
                                        if(err){
                                            console.log(err)
                                        }
                                        else{
                                            cb(null)

                                        }
                                    })
                                }
                            }
                        });
                        
                    }
                })


            }
            else{
                cb(null)
            }

        }],
        uploadImage3:["getUserId",function(cb){
            if( payloadData.images3){
                var profilePic={}
                var id = userId + "4";
                UploadManager.uploadFileToS3WithThumbnail(payloadData.images3, id,0, function(err,result){

                    if(err){
                        console.log("here err",err)
                        cb()
                    }
                    else{
                        profilePic.original =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        profilePic.thumbnail = Config.awsS3Config.s3BucketCredentials.s3URL+result.thumbnail;
                        var query = {
                            "profilePicURL.original": profilePic.original
                        };
                        Service.ImagesService.getImages(query,{},{},function(err,result){
                            if(err){
                                cb(err)
                            }
                            else{
                                if(result.length){
                                    cb(null);
                                }
                                else{
                                    var data = {
                                        profilePicURL:profilePic,
                                        customer:userId
                                    };
                                    console.log(profilePic);
                                    Service.ImagesService.createImages(data,function(err,result){
                                        if(err){
                                            console.log(err)
                                        }
                                        else{
                                            cb(null)

                                        }
                                    })
                                }
                            }
                        });
                       
                    }
                })


            }
            else{
                cb(null)
            }

        }],
        uploadImage4:["getUserId",function(cb){
            if( payloadData.images4){
                var profilePic={}
                var id = userId + "5";
                UploadManager.uploadFileToS3WithThumbnail(payloadData.images4, id,0, function(err,result){

                    if(err){
                        console.log("here err",err);
                        cb()
                    }
                    else{
                        profilePic.original =  Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        profilePic.thumbnail = Config.awsS3Config.s3BucketCredentials.s3URL+result.thumbnail;
                        var query = {
                            "profilePicURL.original": profilePic.original
                        };
                        Service.ImagesService.getImages(query,{},{},function(err,result){
                            if(err){
                                cb(err)
                            }
                            else{
                                if(result.length){
                                    cb(null);
                                }
                                else{
                                    var data = {
                                        profilePicURL:profilePic,
                                        customer:userId
                                    };
                                    console.log(profilePic);
                                    Service.ImagesService.createImages(data,function(err,result){
                                        if(err){
                                            console.log(err)
                                        }
                                        else{
                                            cb(null)

                                        }
                                    })
                                }
                            }
                        });
                       
                    }
                })


            }
            else{
                cb(null)
            }

        }],
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data);
        }
    })
}

var updateSerachProfile = function(payloadData,callback){
    var userId;
    var data=[];
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        updateSearch:['getId',function(cb){
            console.log(payloadData);

            if(parseInt(payloadData.serachMinAge)<=parseInt(payloadData.serachMaxAge)){
                var query = {_id:userId};
                var options = {new:true};
                var setData = {
                    serachMinAge:payloadData.serachMinAge,
                    serachGender:payloadData.serachGender,
                    serachMaxAge:payloadData.serachMaxAge,
                    searchFlieds:true,
                    searchMaximumDistance:payloadData.searchMaximumDistance,
                   // searchMinimumDistance:payloadData.searchMinimumDistance
                };
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err)
                    }else{
                        data = result;
                        cb(null)
                    }
                })
            }
            else{
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_AGE_ENTRY);
            }
        }]

    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data)
        }
    })
}


var updateAllFbFriends = function(payloadData,callback){
    var userId;
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        checkFriend:['getId',function(cb){
            var friendIDs = payloadData.friends;
            var len = friendIDs.length;

            if(len == 0){
                cb(null);
            }
            for(var i =0;i < len;i++){
                (function(i){
                    checkUser(friendIDs[i], userId,function(err,result){
                        if(err){
                            cb(err);
                        }else{
                            if(i == (len -1)){
                                cb(null);
                            }
                        }
                    })
                }(i));
            }
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null);
        }
    })
};



var updateTempFriends = function(payloadData,callback){
    var userId;
    var exist=0;
    var token;
    var type;
    var token1;
    var type1;
    var userName;
    var friendName;
    var facebookIdUser;
    var facebookIdFriend;
    var mongoose = require('mongoose');

    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        getUserIdToken:['getId',function(cb){
            var query = {_id:userId};
            var options = {lean:true};
            var projections = {
                deviceToken:1,
                deviceType:1,
                name:1,
                facebookId:1
            };

            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    token1 = result[0].deviceToken;
                    type1 = result[0].deviceType;
                    userName = result[0].name;
                    facebookIdUser = result[0].facebookId;

                    cb(null);
                }
            })
        }],
        getfriendToken:['getId',function(cb){
            var query = {_id:payloadData.friendsId};
            var options = {lean:true};
            var projections = {
                deviceToken:1,
                deviceType:1,
                name:1,
                facebookId:1
            };

            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    token = result[0].deviceToken;
                    type = result[0].deviceType;
                    friendName = result[0].name;
                    facebookIdFriend = result[0].facebookId;
                    cb(null);
                }
            })
        }],
        checkTempArray:['getId',function(cb){

            var query = {
                _id:payloadData.friendsId,
               // "temp.customer":userId
            };
            var dataToGet = {
                temp:1
            };
           console.log(query);
            var options = {lean:true};
            Service.CustomerService.getCustomer(query,dataToGet,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    console.log("here",result);
                    if(result[0].temp.length){
                        for(var i=0;i<result.length;i++){
                            (function(i){
                                console.log(result[0].temp[i].customer,userId);

                                if(JSON.stringify(result[0].temp[i].customer) === JSON.stringify(userId)){
                                    exist = 1;
                                     console.log("here ssssss",exist)
                                }
                                if( i == result.length-1){
                                    cb(null)
                                }
                            }(i))
                        }

                    }
                    else{
                        cb(null)
                    }
                }
            })
        }],
        updateTempFriend:['getId','checkTempArray',function(cb){
            if(exist==0){
                var query = {_id: userId};
                var options = {lean:true};
                var setData = {
                    $push:{
                        temp:{
                            customer:mongoose.Types.ObjectId(payloadData.friendsId)
                        }
                    }
                };
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        cb(null)
                    }
                })
            }
            else{
               cb(null);
            }
        }],
        removeFromTemp:['updateTempFriend',function(cb){
            if(exist==0){
                cb(null)
            }
            else{
                var query = {_id: payloadData.friendsId};
                var options = {lean:true};
                var setData = {
                    $pull:{
                        temp:{
                            customer:mongoose.Types.ObjectId(userId)
                        }
                    }
                };
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        cb(null)
                    }
                })
            }

        }],
        //removeFromTemp1:['updateTempFriend',function(cb){
        //
        //    var query = {_id: userId};
        //    var options = {lean:true};
        //    var setData = {
        //        $pull:{
        //            temp:{
        //                customer:payloadData.friendsId
        //            }
        //        }
        //    };
        //    Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
        //        if(err){
        //            cb(err);
        //        }else{
        //            cb(null)
        //        }
        //    })
        //}],
        sendToOnGoing:['updateTempFriend','getfriendToken','getUserIdToken',function(cb){
            if(exist==1){
                var query = {_id: userId};
                var options = {lean:true};
                var setData = {
                    $push:{
                        onGoing:{
                            customer:payloadData.friendsId
                        }
                    }
                };
                console.log("===========",query,setData);
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        var message = {
                            "data": {"name": userName,
                                "facebookId":facebookIdUser
                            },
                            "message": "Yor are now friend with " + userName,
                            t:8
                        };

                        if(!(type == "IOS")){

                            NotificationManager.sendAndroidPushNotification(token,message,function(err,result){
                                if(err){
                                    cb(err)
                                }else{
                                    console.log(".............................jnjkjkjkljlk..........................",err,result);
                                    cb(null)
                                }
                            })
                        }else{
                            var payloadData1 = "Yor are now friend with " + userName

                            NotificationManager.sendIosPushNotification(token, message,payloadData1, function(err,result){
                                if(err){
                                    cb(err)
                                } else{
                                    cb(null);
                                }
                            })
                        }
                    }
                })
            }
            else{
                cb(null);
            }
        }],
        sendToOnGoing1:['updateTempFriend','getUserIdToken','getfriendToken',function(cb){
            if(exist==1){
                var query = {_id: payloadData.friendsId};
                var options = {lean:true};
                var setData = {
                    $push:{
                        onGoing:{
                            customer:userId
                        }
                    }
                };
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        var message = {
                            "data": {"name": friendName,
                                "facebookId":facebookIdFriend
                            },
                            "message": "Yor are now friend with " + friendName,
                            t:8
                        };

                        if(!(type1 == "IOS")){

                            NotificationManager.sendAndroidPushNotification(token1,message,function(err,result){
                                if(err){
                                    cb(err)
                                }else{
                                    console.log(".............................jnjkjkjkljlk..........................",err,result);
                                    cb(null)
                                }
                            })
                        }else{
                            var payloadData1 = "Yor are now friend with" + friendName;

                            NotificationManager.sendIosPushNotification(token1, message,payloadData1, function(err,result){
                                if(err){
                                    cb(err)
                                } else{
                                    cb(null);
                                }
                            })
                        }
                    }
                })
            }
            else{
                cb(null);
            }
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null);
        }
    })
};




var checkUser = function(email, userId,callback){

    var flag = 0;
    var userIdFriend;
    var user;
    console.log("flag email flag flag",email);

    async.auto({
        checkUser:function(cb){
            var query ={
                facebookId:email
            };
            var options = {lean:true};
            var projections = {_id:1};
            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    if(result.length){
                        userIdFriend = result[0]._id;
                        flag = 1;
                        console.log("flag flag flag flag",flag,userIdFriend);
                    }
                    cb(null);
                }
            })
        },
        getUserId:['checkUser',function(cb){
            if(flag == 1){
                var query = {
                  //  "friends.$.customer":userIdFriend,
                    _id:userId
                };
                var dataToGet = {friends:1};
                var options = {lean:true};
                Service.CustomerService.getCustomer(query,dataToGet,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        var check=0;
                        console.log("------------a-------------------",result.length);
                        if(result.length){
                            if(result[0].friends.length){
                                for(var i=0;i<result[0].friends.length;i++){
                                    (function(i){
                                        console.log("---------",result[0].friends[i].customer,userIdFriend);

                                        if(JSON.stringify(result[0].friends[i].customer) === JSON.stringify(userIdFriend)){
                                            check=1;
                                        }
                                        if(i==result[0].friends.length -1){
                                            if(check==1){
                                                flag=1;
                                                cb(null)
                                            }
                                            else{
                                                flag=2;
                                                cb(null);
                                            }
                                        }

                                    }(i))
                                }
                            }
                            else{
                                flag=2;
                                cb(null);
                            }

                           // console.log("flag ahahah flag");
                           // flag = 2;
                        }
                        else{
                            cb(null);
                        }
                      //  cb(null);
                    }
                })
            }
            else{
                cb(null);
            }
        }],
        updateUserids:['getUserId',function(cb){
            if(flag == 2){
                console.log("aaaaaaaaaaaaaaaaaa",userId,userIdFriend);
                var query = {
                    _id: userId
                };
                var options = {lean:true};
                var setData = {
                    $push:{
                        friends:{
                            customer:userIdFriend
                        }
                    }
                };
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        cb(null)
                    }
                })
            }
            else{
                    cb(null);
            }
        }]
    },function(err,result){
      if(err){
        callback(err)
      } else{
          callback(null);
      }
    })
}


/*var resetPassword = function (payloadData, callback) {
    var customerObj = null;
    if (!payloadData || !payloadData.email || !payloadData.passwordResetToken || !payloadData.newPassword) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    email: payloadData.email
                };
                Service.CustomerService.getCustomer(criteria, {}, {lean: true}, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            customerObj = userData && userData[0] || null;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                if (customerObj) {
                    if (customerObj.passwordResetToken != payloadData.passwordResetToken) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RESET_PASSWORD_TOKEN);
                    } else {
                        cb();
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                }
            },
            function (cb) {
                if (customerObj) {
                    var criteria = {
                        email: payloadData.email
                    };
                    var setQuery = {
                        password: UniversalFunctions.CryptData(payloadData.newPassword),
                        $unset: {passwordResetToken: 1}
                    };
                    Service.CustomerService.updateCustomer(criteria, setQuery, {}, function (err, userData) {
                        if (err) {
                            cb(err)
                        } else {
                            cb();
                        }
                    })
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};*/

/*ar changePassword = function (queryData, userData, callback) {
    var userFound = null;
    if (!queryData.oldPassword || !queryData.newPassword || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var projection = {};
                var options = {
                    lean: true
                };
                Service.CustomerService.getCustomer(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        if (data && data.length > 0 && data[0]._id) {
                            userFound = data[0];
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                        }
                    }
                })
            },
            function (cb) {
                //Check Old Password
                if (userFound.password != UniversalFunctions.CryptData(queryData.oldPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASS)
                } else if (userFound.password == UniversalFunctions.CryptData(queryData.newPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.SAME_PASSWORD)
                } else {
                    cb();
                }
            },
            function (cb) {
                // Update User Here
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    $set: {
                        firstTimeLogin: false,
                        password: UniversalFunctions.CryptData(queryData.newPassword)
                    }
                };
                var options = {
                    lean: true
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb);
            }

        ], function (err, result) {
            callback(err, null);
        })
    }
};*/


var logoutCustomer = function (userData, callback) {
    if (!userData || !userData._id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userId = userData && userData._id || 1;

        async.series([
            function (cb) {
                //TODO Check Active Bookings Of Customer
                cb();
            },
            function (cb) {
                var criteria = {
                    _id: userId
                };
                var setQuery = {
                    $unset: {
                        accessToken: 1
                    }
                };
                var options = {};
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb);
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};




var getAllFriends = function(payloadData,callback){
    var userId;
    var list;
    var finalData = [];
    var ongo=0;
    console.log("============================",payloadData);
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        checkFriend:['getId',function(cb){
            if(payloadData.friends){
                var friendIDs = payloadData.friends;
                var len = friendIDs.length;

                if(len == 0){
                    cb(null);
                }
                for(var i =0;i < len;i++){
                    (function(i){
                        checkUser(friendIDs[i], userId,function(err,result){
                            if(err){
                                cb(err);
                            }else{
                                if(i == (len -1)){
                                    cb(null);
                                }
                            }
                        })
                    }(i));
                }
            }
            else{
                cb(null);
            }

        }],
        getFrindList:['checkFriend',function(cb){
            var query = {
                _id:userId
            };
            var options = {lean:true};
            var projections = {
                friends:1,
                onGoing:1
            };

            Service.CustomerService.getCustomerByPopulate(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    console.log("===========facebook list==================",result[0]);
                    list = result[0];
                    cb(null);
                }
            })
        }],
        getImageFriends:['getFrindList',function(cb){

            if(list.friends.length){
                for(var i=0;i<list.friends.length;i++){
                    (function(i){

                        var query = {
                            customer:list.friends[i].customer._id,
                            isDeleted:false
                        };

                        Service.ImagesService.getImages(query,{},{lean:true},function(err,result){
                            if(err){
                                cb(err);
                            }
                            else{
                               // console.log("==============================",result);
                                list.friends[i].customer.images =result;
                                if(i==list.friends.length-1){
                                    cb(null);
                                }
                            }
                        })
                    }(i))
                }
            }
            else{
                cb(null);
            }
        }],
        getImageOngoing:['getFrindList',function(cb){
            if(list.onGoing.length){

                for(var i= 0,j=0;i<list.onGoing.length;i++){
                    (function(i){
                        console.log("list.onGoing[i].customer._id",list.onGoing[i].isBlocked);
                        if(list.onGoing[i].isBlocked == false){
                            var query = {
                                customer:list.onGoing[i].customer._id,
                                isDeleted:false
                            };
                            Service.ImagesService.getImages(query,{},{lean:true},function(err,result){
                                if(err){
                                    cb(err);
                                }
                                else{
                                    if(result.length){
                                        console.log("===============a===============",result)
                                        console.log("===============list===============",list)
                                        console.log("==============i===============",i)
                                        console.log("==============customer===============",list.onGoing[i].customer)

                                        list.onGoing[i].customer.images =result;
                                        console.log("==============222222===============",list.onGoing[i]);
                                        console.log("=====================finalData==============",finalData,j);
                                       // finalData.onGoing[j] = {}
                                        finalData.push(list.onGoing[i]);

                                        j++;
                                        ongo=1;
                                        if(i==list.onGoing.length-1){

                                            list.onGoing =  finalData;
                                            cb(null);
                                        }
                                    }else{
                                        cb(null)
                                    }
                                }
                            })
                        }
                        else{
                            if(i==list.onGoing.length-1){
                                if(ongo==1){
                                    list.onGoing =  finalData.onGoing;
                                }
                                else{
                                    list.onGoing=[];
                                }
                                cb(null);
                            }
                        }
                    }(i))
                }

            }
            else{
                cb(null);
            }
        }]
        // onGoingList:['getFrindList',function(cb){
        //     var tempOnGoing = list;
        //     var len = tempOnGoing.length;
        //     for(var i = 0;i<len;i++){
        //         (function(i){
        //             getFriendsOngoing(tempOnGoing[i].onGoing,function(err,result){
        //                 if(err){
        //                     cb(err);
        //                 }else{
        //                     if(i == (len -1)){
        //                         cb(null);
        //                     }
        //                 }
        //             })

        //         }(i));
        //     }
        // }],
        // friendsList:['getFrindList',function(cb){
        //     var tempOnGoing = list;
        //     var len = tempOnGoing.length;
        //     for(var i = 0;i<len;i++){
        //         (function(i){
        //             getFriends(tempOnGoing[i].friends,function(err,result){
        //                 if(err){
        //                     cb(err);
        //                 }else{
        //                     if(i == (len -1)){
        //                         cb(null);
        //                     }
        //                 }
        //             })

        //         }(i));
        //     }
        // }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,list)
        }
    })
}

// function getFriendsOngoing(onGoing,callback){

// var query = {

// }

// }

// function getFriends(friends,callback){

// }

var explore = function(payloadData,callback){
    var  userId;
    var serachMinAge;
    var serachMaxAge;
    var serachGender;
    var findData;
    var data123=[];
    var searchMaximumDistance;
    var searchMinimumDistance;
    var flag = false;
    var faceBookFriends=[];
    var onGoingFriends=[];
    var tempFriends=[];
    var count=0;
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        getDetails:['getId',function(cb){
            var query = {_id:userId};
           var options = {lean:true};
            var projections = {
                serachMinAge:1,
                serachMaxAge:1,
                serachGender:1,
                searchMaximumDistance:1
            };

            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                  // console.log("==================",result);
                    if(result.length){
                        serachMinAge = result[0].serachMinAge;
                        serachMaxAge = result[0].serachMaxAge;
                        serachGender =    result[0].serachGender;
                     //   searchMinimumDistance = result[0].searchMinimumDistance *1000;
                        searchMaximumDistance = result[0].searchMaximumDistance *1000;
                        cb(null);
                    }
                    else{
                        cb(null);
                    }
                }
            })
        }],
        getAllFbFriends:['getId',function(cb){
           var query = {
               _id:userId
           };
            var dataToGet ={
                friends:1
            };
            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(null)
                }
                else{
                    if(result[0].friends.length){
                   //     console.log(result[0].friends.length);

                        for(var i=0;i<result[0].friends.length;i++){
                            (function(i){
                                faceBookFriends[i] = result[0].friends[i].customer;
                                if(i==result[0].friends.length-1){
                                    cb(null);
                                }

                            }(i))
                        }
                    }
                    else{
                        cb(null);
                    }
                }
            })
        }],
        getAllOnGoingFriends:['getId',function(cb){
            var query = {
                _id:userId
            };
            var dataToGet ={
                onGoing:1
            };
            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(null)
                }
                else{
                    if(result[0].onGoing.length){
                        console.log(result.length);

                        for(var i=0;i<result[0].onGoing.length;i++){
                            (function(i){
                                onGoingFriends[i] = result[0].onGoing[i].customer;
                                if(i==result[0].onGoing.length-1){
                                    cb(null);
                                }
                            }(i))
                        }
                    }
                    else{
                        cb(null);
                    }
                }
            })
        }],
        getAllTempFriends:['getId',function(cb){
            var query = {
                _id:userId
            };
            var dataToGet ={
                temp:1
            };
            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(null)
                }
                else{
                    if(result[0].temp.length){
                        //console.log(result.length);

                        for(var i=0;i<result[0].temp.length;i++){
                            (function(i){
                                tempFriends[i] = result[0].temp[i].customer;
                                if(i==result[0].temp.length-1){
                                    cb(null);
                                }

                            }(i))
                        }
                    }
                    else{
                        cb(null);
                    }
                }
            })
        }],

        serachPerson:['getDetails','getAllFbFriends','getAllOnGoingFriends','getAllTempFriends',function(cb){

                var skip= payloadData.skip *5;
                var options = {skip:skip,limit:5};
            var array3 = faceBookFriends.concat(onGoingFriends);
                array3 = array3.concat(tempFriends);
     //       console.log("========concat facebook and on going==========",array3);
                if(serachGender==Config.APP_CONSTANTS.DATABASE.serachGender.BOTH){
                    var query = {
                        currentLocation: {
                            $nearSphere: [payloadData.long,payloadData.lat],
                            $maxDistance: searchMaximumDistance
                        },
                        age:{$gte:serachMinAge ,$lte:serachMaxAge},
                        _id:{$ne:userId,$nin:array3},
                        snake:{$lte:5}
                    }
                }
                else{
                    var query = {
                        $and:[
                            {snake:{$lte:5}},
                            {age:{$gte:serachMinAge ,$lte:serachMaxAge}},
                            {gender:serachGender},
                            {_id:{$ne:userId,$nin:array3}},

                            {currentLocation:
                            { $nearSphere : [ payloadData.long,payloadData.lat] ,
                                $maxDistance: searchMaximumDistance
                            }
                    }
                        ]
                    };
                }

                var projections= {
                };

                Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                    if(err){
                        console.log(err);
                        cb(err);
                    }else{

                        if(result.length){
                          //  console.log("==========================",result);
                            findData = result;
                            flag  = true;
                            cb(null)
                        }
                        else{
                            flag=false;
                            cb(null);
                        }
                    }
                })
        }],
        getImage:['serachPerson',function(cb){
            if(flag==true){
                for(var i=0;i<findData.length;i++){
                    (function(i){
                        var query = {
                            customer:findData[i]._id,
                            isDeleted:false
                        };
                        var options = {lean:true};
                        var projections = {
                            profilePicURL:1
                        };
                        Service.ImagesService.getImages(query,projections,{limit:1},function(err,result){
                            if(err){
                                cb(err)
                            }else{
                                if(result.length){
                            //        console.log("=============images==========",result);
                                    data123.push({
                                        data:findData[i],
                                        images:result
                                    });
                                  //  findData[i].Images = result;
                                    if(i==findData.length -1)
                                    cb(null);
                                }else{
                                    data123.push({
                                        data:findData[i],
                                        images:result



                                    });
                                   // findData[i].Images = [];
                            //        console.log("=============images==========",result);
                                    if(i==findData.length -1)
                                    cb(null)
                                }
                            }
                        })
                    }(i))
                }
            }
            else{
                cb(null);
            }
        }]
    },function(err,result){
        if(err){
            callback(err)
        }else{
            if(flag==true){
                callback(null,data123)
            }
            else{
                callback(null,[])
            }
        }
    })
};


var UserDetails = function(payloadData,callback){
    var userId;
    var data;

    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        serachPerson:['getId',function(cb){
            var options = {lean:true};
            var query = {
                _id:payloadData.userId
            };
            var projections= {};

            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    cb(null);
                }else{
                    data = result[0];
                    cb(null)
                }
            })
        }],
        getImage:['serachPerson',function(cb){
            var query = {customer:data._id,
                isDeleted:false
            };
            var options = {lean:true};
            var projections = {
                profilePicURL:1
            };
            Service.ImagesService.getImages(query,projections,options,function(err,result){
                if(err){
                    cb(err)
                }else{
                    if(result.length){
                        data.Images = result;
                        cb(null)
                    }else{
                        data.Images = [];
                        cb(null)
                    }
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data)
        }
    })
};


var getAllGames = function(payloadData,callback){
    var data={};
    var turnbase;
    var intractive;
  //  var turnbase = [{gameName:"Image Game",gameId:"127321378",image:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg"},
    //    {gameName:"Matching Game",gameId:"4345433",image:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg"}];
   // var intractive = ["TIC-TAK-TOO","GUESS"];
   // var intractive = [{gameName:"TIC-TAK-TOO",gameId:"534353",image:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg"},
     //   {gameName:"GUESS",gameId:"544422",image:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg"}];
    async.auto({
        getGamesturnbase:function(cb){
            var query = {gameType:Config.APP_CONSTANTS.DATABASE.GAMETYPE.TURNBASE};
            var options = {lean:true};
            var projections = {
            };
            Service.GamesService.getGames(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    turnbase=result;
                        //data.intractive=intractive;

                    //console.log(data);
                    cb(null)
                }
            })
        },
        getGames:function(cb){
            var query = {gameType:Config.APP_CONSTANTS.DATABASE.GAMETYPE.INTERACTIVE};
            var options = {lean:true};
            var projections = {
            };

            Service.GamesService.getGames(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    intractive=result;
                    //data.intractive=intractive;

                    //console.log(data);
                    cb(null)
                }
            })
        }

    },function(err,result){
        if(err){
            callback(err);
        }else{
            data.turnbase=turnbase;
            data.intractive=intractive;
            callback(null,data);
        }
    })
};


var requestNewGames = function(payloadData,callback){
    var userId;
    var  gameId;
    var name;
    var deviceType;
    var gameName;
    var uniqueName;
    var gameImage;
    var deviceToken;
    console.log("payloadData",payloadData);
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        checkSnackCount:['getId',function(cb){

            var query = {
                _id:payloadData.requestUserId,
                snake:{$lt:6}
            };
            var dataToGet = {
            };

            Service.CustomerService.getCustomer(query,dataToGet,{},function(err,result){
                if(err){
                    cb(null);
                }
                else{
                    if(result.length){
                        cb(null);
                    }
                    else{
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.SNAKE);
                    }
                }
            })

        }],
        gameUpdate:['checkSnackCount',function(cb){
            var date= new Date();
            var obj = {
                sender:userId,
                receiver:payloadData.requestUserId,
                games:payloadData.gameId,
                wager:payloadData.wagerId,
                requestTime:date
            };
            Service.RequestGamesService.createRequestGames(obj,function(err,result){
                if(err){
                    console.log(err);
                    cb(err);
                }else{
                    gameId = result._id;
                    cb(null);
                }
            })
        }],
        getRequestUserDetails:function(cb){

            var query = {
                _id:payloadData.requestUserId
            } ;
            var dataToGet = {
                deviceToken:1,
                deviceType:1
            };

            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                        console.log(result);
                        deviceType = result[0].deviceType;
                        deviceToken = result[0].deviceToken;
                        cb(null);
                }
            })
        },
        saveSms:['gameUpdate',function(cb){
            var data={};
            data.sender = userId;
            data.receiver = payloadData.requestUserId;
            data.sendTime = payloadData.time;
            data.message_type = 0;
            data.message = "Request have been created for  " + payloadData.gameName;
            data.status = Config.APP_CONSTANTS.DATABASE.MEG_STATUS.REACHSERVER;
            Service.ChatService.createChats(data,function(err,result){
                if(err){
                    console.log("helol=========err============",err);
                    cb(err);
                }else{
                    console.log("....reachserver..");
                   // id = result._id;
                    cb(null);
                }
            })
        }],
        //updateWagerCount:function(cb){
        //
        //    var query = {
        //        _id:payloadData.wagerId
        //    } ;
        //    var dataToGet = {
        //     $inc: { quantity: -1}
        //    };
        //
        //    Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
        //        if(err){
        //            cb(err);
        //        }
        //        else{
        //            console.log(result);
        //            deviceType = result[0].deviceType;
        //            deviceToken = result[0].deviceToken;
        //            cb(null);
        //        }
        //    })
        //},
        getName:function(cb){
            var query = {
                _id:payloadData.gameId
            };
            var dataToGet = {
                uniqueName:1,
                gameName:1,
                gameImages:1
            }

            Service.GamesService.getGames(query,dataToGet,{},function(err,result){
                if(err) {
                    cb(err)
                }
                else{
                    uniqueName = result[0].uniqueName;
                    gameName = result[0].gameName;
                    gameImage = result[0].gameImages;
                    cb(null);
                }
            })

        },
        getOtherUserName:['gameUpdate',function(cb){
            var query = {_id:userId};
            var options = {lean:true};
            var projections = {
                name:1
            };

            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    name = result[0].name;
                    cb(null);
                }
            })
        }],
        sendNotification:['getOtherUserName','gameUpdate','getName','getRequestUserDetails',function(cb){

            var message = {
                "data": {"gameId": payloadData.gameId,
                    "sender":userId,
                    "wager":payloadData.wagerId,
                    "requestId":gameId,
                    "uniqueName":uniqueName,
                    "name":name,
                    gameImage:gameImage
                },
                "message": "You Have A New Game of " +gameName + ' created by ' + name,
                t:3
            };

            if(!(deviceType == "IOS")){

                NotificationManager.sendAndroidPushNotification(deviceToken, message, function(err,result){
                    if(err){
                        cb(err)
                    } else{
                        cb(null);
                    }
                })
            }
            else{
                var payloadData1 = "You Have A New Game";
                NotificationManager.sendIosPushNotification(deviceToken, message,payloadData1, function(err,result){
                    if(err){
                        cb(err)
                    } else{
                        cb(null);
                    }
                })
            }
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null, gameId)
        }
    })
};


var requestAccepted = function(payloadData,callback){
    var userId;
    var exists=0;
    var  gameId;
    var deviceType;
    var deviceToken;
    var uniqueName;
    var gameName;
    var exists1=0;
    var name;
    var gameImage;
    var mongoose = require('mongoose');
    console.log("==============================payloadData=[====================",payloadData);
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        gameUpdate:['getId',function(cb){
            var date = new Date();
            var obj = {
                _id:payloadData.gameId
            };
            var dataToSet = {
                is_accpeted:true,
                accpectTime:date,
                matchStatus:"LIVE"
            };
            Service.RequestGamesService.updateRequestGames(obj,dataToSet,{new:true},function(err,result){
                if(err){
                    console.log(err);
                    cb(err);
                }else{
                    console.log("=======================result===================",result)
                    gameId = result.games;
                    cb(null);
                }
            })
        }],
        onGoingUserCheck:['getId',function(cb){

            var query = {
                _id:payloadData.senderId
                // "temp.customer":userId
            };
            var dataToGet = {
                onGoing:1
            };
           // console.log(query);
            var options = {lean:true};
            Service.CustomerService.getCustomer(query,dataToGet,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    console.log("here",result);
                    if(result[0].onGoing.length){
                        for(var i=0;i<result[0].onGoing.length;i++){
                            (function(i){
                                console.log(result[0].onGoing[i].customer,userId);
                                if(JSON.stringify(result[0].onGoing[i].customer) === JSON.stringify(userId)){
                                    exists = 1;
                                  //  console.log("here ssssss",exist)
                                }
                                if( i == result.length-1){
                                    cb(null);
                                }
                            }(i))
                        }
                    }
                    else{
                        cb(null);
                    }
                }
            })
        }],
        //onGoingUserCheck1:['getId',function(cb){
        //
        //    var query = {
        //        _id:payloadData.senderId,
        //        "onGoing.$.customer":userId
        //    };
        //    var dataToGet = {
        //        onGoing:1
        //    };
        //    var options = {lean:true};
        //    Service.CustomerService.getCustomer(query,dataToGet,options,function(err,result){
        //        if(err){
        //            cb(err);
        //        }else{
        //            if(result.length){
        //                exists = 1;
        //            }
        //            cb(null)
        //        }
        //    })
        //}],
        insertToOnGoing:['onGoingUserCheck',function(cb){

            if(exists==0){
                var query = {_id: userId};
                var options = {new:true};
                var setData = {
                    $push:{
                        onGoing:{
                            customer:mongoose.Types.ObjectId(payloadData.senderId)
                        }}
                };
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                       // name = result.name;
                        cb(null)
                    }
                })
            }
            else{
                cb(null);
            }
        }],
        insertToOnGoing1:['onGoingUserCheck',function(cb){

            if(exists==0){
                var query = {_id: payloadData.senderId};
                var options = {lean:true};
                var setData = {
                    $push:{
                        onGoing:{
                            customer:mongoose.Types.ObjectId(userId)
                        }
                    }
                };
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        cb(null)
                    }
                })
            }
            else{
                cb(null);
            }
        }],
        getRequestUserDetails:function(cb){
            var query = {
                _id:payloadData.senderId
            };
            var dataToGet = {
                deviceToken:1,
                deviceType:1
            };

            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                    console.log("======================device token======================== ",result);
                    deviceType = result[0].deviceType;
                    deviceToken = result[0].deviceToken;
                    cb(null);
                }
            })
        },
        getName:['gameUpdate',function(cb){
            var query = {
                _id:gameId
            };
            var dataToGet = {
                uniqueName:1,
                gameName:1,
                gameImages:1
            };

            Service.GamesService.getGames(query,dataToGet,{},function(err,result){
                if(err) {
                    cb(err)
                }
                else{
                    console.log("===============uniqueName===============",result);
                    uniqueName = result[0].uniqueName;
                    gameName = result[0].gameName;
                    gameImage = result[0].gameImages;
                    cb(null);
                }
            })

        }],
        details:['getId',function(cb){

            var query = {
                _id:userId
            }
            var options = {lean:true};
            var projections = {name:1};
            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    console.log(err);
                    callback(err);
                }else{
                  name = result[0].name;
                    cb(null);
                }
            })

        }],
        sendNotification:['gameUpdate','details','getName','getRequestUserDetails',function(cb){
            console.log("==============================here=[====================");

             var message = {
                "data": {
                    "requestId": payloadData.gameId,
                    uniqueName:uniqueName,
                    gameId:gameId,
                    "owner":userId,
                    name:name,
                    gameImage:gameImage
                },
                "message": gameName + " Request accepted by " + name,
                 t:4
            };
            console.log("==============================message=====================",message);
            if (deviceType == 'IOS') {
                var payloadData1 = "Request accepted ";
                NotificationManager.sendIosPushNotification(deviceToken, message,payloadData1, function(err,result){
                    if(err){
                        cb(err)
                    } else{
                        cb(null);
                    }
                })
            }
            else {
                console.log("==============================deviceToken=[====================",deviceToken);

                var token = [];
                token[0] = {deviceToken: deviceToken};
               // token[0] = deviceToke;
                NotificationManager.sendAndroidPushNotification(deviceToken, message, function(err,result){
                    cb(null)
                })
            }
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null, gameId)
        }
    })
};



var requestRejected = function(payloadData,callback){
    var userId;
    var  gameId;
    var deviceType;
    var uniqueName;
    var gameName;
    var name;
    var deviceToken;
    console.log("payloadData",payloadData);
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        userDetails:['getId',function(cb){
            var query = {
                _id:userId
            }
            var options = {lean:true};
            var projections = {name:1};
            Service.CustomerService.getCustomer(query,projections,options,function(err,result) {
                if (err) {
                    console.log(err)
                    callback(err);
                } else {
                    name = result[0].name;
                    cb(null)
                }
            })
        }],
        gameUpdate:['getId',function(cb){
            var date = new Date();
            var obj = {
                _id:payloadData.gameId
            };
            var dataToSet = {
                rejectTime:date,
                matchStatus:"REJECT"
            };
            Service.RequestGamesService.updateRequestGames(obj,dataToSet,function(err,result){
                if(err){
                    console.log(err);
                    cb(err);
                }else{
                    gameId = result.games;
                    cb(null);
                }
            })
        }],
        getRequestUserDetails:function(cb){

            var query = {
                _id:payloadData.senderId
            };
            var dataToGet = {
                deviceToken:1,
                deviceType:1
            };

            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                    // console.log(result);
                    deviceType = result[0].deviceType;
                    deviceToken = result[0].deviceToken;
                    cb(null);
                }
            })

        },
        getName:['gameUpdate',function(cb){
            var query = {
                _id:gameId
            };
            var dataToGet = {
                uniqueName:1,
                gameName:1
            };

            Service.GamesService.getGames(query,dataToGet,{},function(err,result){
                if(err) {
                    cb(err)
                }
                else{
                    console.log("===============uniqueName===============",result);
                    uniqueName = result[0].uniqueName;
                    gameName = result[0].gameName;
                    cb(null);
                }
            })

        }],
        sendNotification:['gameUpdate','getRequestUserDetails','userDetails',function(cb){

            var message = {
                "data": {"game": payloadData.requestId,
                    "sender":userId
                },
                "message": gameName + " Request Rejected by " + name,
                t:5
            };
            if (deviceType == 'IOS') {
                var payloadData1 = "Request Rejected ";
                NotificationManager.sendIosPushNotification(deviceToken, message,payloadData1, function(err,result){
                    if(err){
                        cb(err)
                    } else{
                        cb(null);
                    }
                })
            }
            else {
                var token = [];
                var deviceToke = {deviceToken: deviceToken};
                token[0] = deviceToke;
                NotificationManager.sendAndroidPushNotification(deviceToken, message, function(err,result){
                    cb(null);
                })
            }
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null, gameId);
        }
    })
};



var winnerGames = function(payloadData,callback){
    var userId;
    var deviceType;
    var deviceToken;
    var gameId;
    var name;
    var uniqueName;
    var gameName;
    async.auto({

        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        winnerIdUpdate:['getId',function(cb){
            var query = {
                _id:payloadData.gameId
            };
            var options = {lean:true};
            var setData = {
                winUser:userId,
                matchStatus:"CLOSE",
                lossUser:payloadData.anotherUserId,
                gameEndTime:new Date()
            };
            Service.RequestGamesService.updateRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    gameId  = result.games;
                    cb(null);
                }
            })
        }],
        userDetails:['getId',function(cb){
            var query = {
                _id:userId
            };
            var options = {lean:true};
            var projections = {name:1};
            Service.CustomerService.getCustomer(query,projections,options,function(err,result) {
                if (err) {
                    console.log(err);
                    callback(err);
                } else {
                    name = result[0].name;
                    cb(null)
                }
            })
        }],
        getName:['winnerIdUpdate',function(cb){
            var query = {
                _id:gameId
            };
            var dataToGet = {
                uniqueName:1,
                gameName:1
            };

            Service.GamesService.getGames(query,dataToGet,{},function(err,result){
                if(err) {
                    cb(err)
                }
                else{
                    console.log("===============uniqueName===============",result);
                    uniqueName = result[0].uniqueName;
                    gameName = result[0].gameName
                    cb(null);
                }
            })

        }],
        getRequestUserDetails:function(cb){
            var query = {
                _id:payloadData.anotherUserId
            };
            var dataToGet = {
                deviceToken:1,
                deviceType:1
            };

            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                    console.log("======================device token======================== ",result);
                    deviceType = result[0].deviceType;
                    deviceToken = result[0].deviceToken;
                    cb(null);
                }
            })
        },
        sendNotification:['winnerIdUpdate','userDetails','getName','getRequestUserDetails',function(cb){
            console.log("==============================here=[====================");

            var message = {
                "data": {"game": payloadData.gameId,
                    "sender":userId
                },
                "message": "You have lost " + gameName + "played with " + name,
                t:7
            };
            if (deviceType == 'IOS') {
                var payloadData1 = "lose Game";
                NotificationManager.sendIosPushNotification(deviceToken, message,payloadData1, function(err,result){
                    if(err){
                        cb(err)
                    } else{
                        cb(null);
                    }
                })
            }
            else {
                console.log("==============================deviceToken=[====================",deviceToken);

                var token = [];
                token[0] = {deviceToken: deviceToken};
                // token[0] = deviceToke;
                NotificationManager.sendAndroidPushNotification(deviceToken, message, function(err,result){
                    cb(null)
                })
            }
        }]

    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null)
        }
    })
};



var loseGames = function(payloadData,callback){
    var userId;
    var deviceToken;
    var name;
    var uniqueName;
    var gameName;
    var gameId;
    var deviceType;
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        winnerIdUpdate:['getId',function(cb){
            var query = {
                _id:payloadData.gameId
            };
            var options = {new:true};
            var setData = {
                    lossUser:userId,
                    matchStatus:"CLOSE",
                    gameEndTime:new Date(),
                    winUser:payloadData.anotherUserId
                };
            Service.RequestGamesService.updateRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    console.log("==================image=================",result);
                    gameId = result.games;
                    SocketManager.giveUp(payloadData,function(err,result){

                        if(err){
                            cb(err)
                        }
                        else{
                            cb(null);
                        }
                    })
                }
            })
        }],
        userDetails:['getId',function(cb){
            var query = {
                _id:userId
            };
            var options = {lean:true};
            var projections = {name:1};
            Service.CustomerService.getCustomer(query,projections,options,function(err,result) {
                if (err) {
                    console.log(err);
                    callback(err);
                } else {
                    name = result[0].name;
                    cb(null)
                }
            })
        }],
        getName:['winnerIdUpdate',function(cb){
            var query = {
                _id:gameId
            };
            var dataToGet = {
                uniqueName:1,
                gameName:1
            };

            Service.GamesService.getGames(query,dataToGet,{},function(err,result){
                if(err) {
                    cb(err)
                }
                else{
                    console.log("===============uniqueName===============",result);
                    uniqueName = result[0].uniqueName;
                    gameName = result[0].gameName;
                    cb(null);
                }
            })

        }],
        getRequestUserDetails:function(cb){
            var query = {
                _id:payloadData.anotherUserId
            };
            var dataToGet = {
                deviceToken:1,
                deviceType:1
            };

            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                    console.log("======================device token======================== ",result);
                    deviceType = result[0].deviceType;
                    deviceToken = result[0].deviceToken;
                    cb(null);
                }
            })
        },
        sendNotification:['winnerIdUpdate','getName','userDetails','getRequestUserDetails',function(cb){
            console.log("==============================here=[====================");

            var message = {
                "data": {"game": payloadData.gameId,
                    "sender":userId
                },
                "message": "You won " + gameName + "played with " + name,
                t:6
            };
            if (deviceType == 'IOS') {
                var payloadData1 = "Win Game";
                NotificationManager.sendIosPushNotification(deviceToken, message,payloadData1, function(err,result){
                    if(err){
                        cb(err)
                    } else{
                        cb(null);
                    }
                })
            }
            else {
                console.log("==============================deviceToken=[====================",deviceToken);

                var token = [];
                token[0] = {deviceToken: deviceToken};
                // token[0] = deviceToke;
                NotificationManager.sendAndroidPushNotification(deviceToken, message, function(err,result){
                    cb(null)
                })
            }
        }]

    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null)
        }
    })
};




var tieGame = function(payloadData,callback){
    var userId;
    async.auto({

        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        tieIdUpdate:['getId',function(cb){
            var query = {
                _id:payloadData.gameId
            };
            var options = {lean:true};
            var setData = {
                matchStatus:"TIE"
            };
            Service.RequestGamesService.updateRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    cb(null)
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null);
        }
    })
};


var rematchGame = function(payloadData,callback){
    var userId;
    var data;
    var deviceType;
    var deviceToken;
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        rematchGames:['getId',function(cb){
            var query = {
                _id:payloadData.gameId
            };
            var options = {new:true};
            var setData = {
                matchStatus:"REMATCH",
                rematchRequest:true
            };
            Service.RequestGamesService.updateRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    data = result;
                    cb(null)
                }
            })
        }],
        getRequestUserDetails:function(cb){

            var query = {
                _id:payloadData.sendToId
            };
            var dataToGet = {
                deviceToken:1,
                deviceType:1
            };

            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                    // console.log(result);
                    deviceType = result[0].deviceType;
                    deviceToken = result[0].deviceToken;
                    cb(null);
                }
            })

        },
        sendNotification:['rematchGames','getRequestUserDetails',function(cb){

             var message = {
                "data": {"game": payloadData.requestId,
                    "sender":userId
                },
                "message": "Requested Rematch ",
                 t:2
            };
            if (deviceType == 'IOS') {
                var payload = "Requested Rematch ";
                NotificationManager.sendIosPushNotification(deviceToken, message, payload, cb)
            }

            else {
                var token = [];
                 token[0] = {deviceToken: deviceToken};
               // token[0] = deviceToke;
                NotificationManager.sendAndroidPushNotification(deviceToken, message, function(err,result){
                    cb(null)
                })
            }
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data)
        }
    })
};


var changeWager = function(payloadData,callback){
    var userId;
    var data;
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        rematchGames:['getId',function(cb){
            var query = {
                _id:payloadData.gameId
            }
            var options = {new:true};
            var setData = {
                wager:payloadData.wager
            }
            Service.RequestGamesService.updateRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                   // data = result;
                    cb(null)
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,{})
        }
    })
}



var imSnake = function(payloadData,callback){
    var userId;
    var data;
    var deviceType;
    var deviceToken;

    console.log("=================a==========================",payloadData)
    async.auto({

        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        imSnakeupdate:['getId',function(cb){
            var query = {
                _id:payloadData.gameId
            }
            var options = {new:true};
            var setData = {
                iAmSnake:true
            }
            Service.RequestGamesService.updateRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    console.log("=================ssssssss=========================",result,userId)
                    cb(null)
                }
            })
        }],
        updatesnake:['imSnakeupdate',function(cb){
            var query = {
                _id : userId
            };
            var options = {new:true};
            var setData = {
                $inc:{snake:1},
                lastSnake:new Date().getTime()
            };
            Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    console.log("=================aaaaaaaaaaa==========================",result)
                    data = result;
                    cb(null)
                }
            })
        }],
        getRequestUserDetails:function(cb){

            var query = {
                _id:payloadData.anotherUserId
            };
            var dataToGet = {
                deviceToken:1,
                deviceType:1
            };

            Service.CustomerService.getCustomer(query,dataToGet,{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                    // console.log(result);
                    deviceType = result[0].deviceType;
                    deviceToken = result[0].deviceToken;
                    cb(null);
                }
            })

        },
        sendNotification:['updatesnake','getRequestUserDetails',function(cb){

            var message = {
                "data": {"game": payloadData.gameId,
                    "sender":userId
                },
                "message": "I am snake",
                t:2
            };
            if (deviceType == 'IOS') {
                var payload = "I am snake";
                NotificationManager.sendIosPushNotification(deviceToken, message, payload, cb)
            }

            else {
                var token = [];
                token[0] = {deviceToken: deviceToken};
                // token[0] = deviceToke;
                NotificationManager.sendAndroidPushNotification(deviceToken, message, function(err,result){
                    cb(null)
                })
            }
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data)
        }
    })
}






var getIdByToken  = function(token,callback){
    var query = {
        accessToken:token
    }
    var options = {lean:true};
    var projections = {_id:1};
    Service.CustomerService.getCustomer(query,projections,options,function(err,result){
        if(err){
            console.log(err)
            callback(err);
        }else{
            if(result.length){
                callback(null,result[0]._id)
            }else{
                callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED)
            }
        }
    })
}


var getAllWager= function(payloadData,callback){
   var data;
    async.auto({
        getWager:function(cb){
            var query = {is_deleted : false};
            var options = {lean:true};
            var projections = {};
            Service.WagerService.getWages(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    data  = result;
                    cb(null)
                }
            })
        }
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data);
        }
    })
}



var logout= function(payloadData,callback){
    var data;
    var userId;
    async.auto({

        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        logoutCustomer:['getId',function(cb){
            var query = {
                _id : userId
            };
            var options = {new:true};
            var setQuery = {
                $unset: {
                    deviceToken:1
                }
            };
            Service.CustomerService.updateCustomer(query,setQuery,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    data = result;
                    cb(null)
                }
            })

        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data)
        }
    })
};


var deleteImage= function(payloadData,callback){
    var data;
    var userId;
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        deleteImages:['getId',function(cb){
            var query = {
                _id : userId
            };
            var options = {new:true};
            var setQuery = {
               isDeleted:true
            };
            Service.ImagesService.updateImages(query,setQuery,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                   // data = result;
                    cb(null)
                }
            })

        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data)
        }
    })
};
var checkRegisteredUser = function(payloadData,callback)
{
    var userId;
    var profile;
    async.auto({

        fbId:function(cb){
            var query = {
                facebookId : payloadData.fbId
            };
            var options = {new:true};

            Service.CustomerService.getCustomer(query,{},options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    if(result.length){

                        profile=result;
                       // console.log(profile);
                        cb(null)
                    }
                    else{
                        profile=0;
                        cb(null);
                    }

                }
            })
        },
        updateToken:['fbId',function(cb){
            if(profile==0){
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
            }
            else{
                    var query = {
                        facebookId : payloadData.fbId
                    };
                var dataToSet = {
                    deviceType:payloadData.deviceType,
                    deviceToken:payloadData.deviceToken
                };
                var options = {new:true};
                Service.CustomerService.updateCustomer(query,dataToSet,options,function(err,result){
                    if(err){
                        cb(err);
                    }
                    else{
                        cb(null);
                    }
                })
            }
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,profile);

            //accessToken: accessToken,
            //    profileUpdated:dataReturn,
            //    userId:id
        }
    })
};

var sendToOnGoing = function(payloadData,callback){

    var flag = false;
    var userId;

    async.auto({
        checkUser:function(cb){
            var query ={
                accessToken:payloadData.accessToken
            };
            var options = {lean:true};
            var projections = {_id:1};
            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }
                else{
                    if(result.length){
                        userId = result[0]._id;
                        flag = true;
                        console.log(result[0]._id,flag)
                        cb(null);
                    }
                    else{
                        cb(null);
                    }

                }
            })
        },
        updateUserids:['checkUser',function(cb){
            if(flag == true){
                var query = {_id: userId};
                var options = {lean:true};
                var setData = {
                    $push:{
                        onGoing:{
                            customer:payloadData.receiver
                        }
                    }
                };
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        cb(null)
                    }
                })
            }
            else{
                cb(null);
            }
        }],
        updatereciver:['checkUser',function(cb){
            if(flag == true){
                var query = {_id:payloadData.receiver};
                var options = {lean:true};
                var setData = {
                    $push:{
                        onGoing:{
                            customer:userId
                        }
                    }
                };
                Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                    if(err){
                        cb(err);
                    }else{
                        cb(null)
                    }
                })
            }
            else{
                cb(null);
            }
        }],
        chatMessage:['checkUser', function (cb) {
            if(flag == true) {
                var date = new Date().getTime();
                var query = {_id: userId};
                var options = {lean: true};
                var setData = {
                    sender: userId,
                    receiver: payloadData.receiver,
                    message: "hello",
                    sendTime: date
                };
                Service.ChatService.createChats(setData, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null)
                    }
                })
            }
            else{
                cb(null)
            }

        }]
    },function(err,result){
        if(err){
            callback(err)
        } else{
            callback(null);
        }
    })
};



var blockOngoing = function (payloadData,callback) {
    var userId;
    var flag;
    auto.async({

        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        blockOnGoingFriend:['getId',function(cb){

            var query = {
                "onGoing._id":payloadData.onGoingId
            };
            var dataToSet ={
                "onGoing.$.isBlocked":true
            };

            Service.CustomerService.updateCustomer(query,dataToSet,{},function(err,result){
                if(err){
                    cb(err)
                }
                else{
            cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            cb(null,{})
        }
    })
}

var blockOngoing = function (payloadData,callback) {

    var userId;

    var flag;

    auto.async({

        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        blockOnGoingFriend:['getId',function(cb){

            var query = {
                "onGoing._id":payloadData.onGoingId
            };
            var dataToSet ={
                "onGoing.$.isBlocked":payloadData.status
            };

            Service.CustomerService.updateCustomer(query,dataToSet,{},function(err,result){
                if(err){
                    cb(err)
                }
                else{
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            cb(null,{})
        }
    })
};


var getAllChat = function(payloadData,callback){
    var chatData;
    var chatData1;
    var userId;

    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        chatofusers:['getId',function(cb){
            var query =  {
                "$match": {
                    "receiver": userId
                }
            };

            var setData={
                "$group": {
                    "_id": "$sender",
                    "message": {
                        "$first": "$message"
                    },
                    "sendTime": {
                        "$first": "$sendTime"
                    },
                    "status":{
                        "$first": "$status"
                    }
                }
            };

            var project= {
                "$project": {
                    "_id": 1,
                    "sender": "$_id",
                    "message": 1,
                    "sendTime": 1,
                    "status":1
                }
            };
            var options = {"$sort":{sendTime:-1}};

            Service.ChatService.getAllChating(query,setData,project,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    chatData= result;
                    cb(null);
                }
            })
        }],
        getLastMessage:['chatofusers1','chatofusers',function(cb){
            cb(null)
        }]

        //chat:['getId',function(cb){
        //    var query = {
        //        $or:[{sender:userId,
        //        receiver:userId}]
        //    };
        //    var dataToGet = {
        //    };
        //    Service.ChatService.getChats(query,dataToGet,{lean:true},function(err,result){
        //        if(err){
        //            cb(null);
        //        }
        //        else{
        //            chatData =result;
        //            cb(null);
        //        }
        //    })
        //
        //}]

    },function(err,result){
        if(err){
            callback(null)
        }
        else{
            callback(null,chatData1)
        }
    })

};

var getIndivisualChat = function(payloadData,callback){
    var userId;
    var date;

    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        chatofuser:['getId',function(cb){
            var mongoose = require('mongoose');
            var query;
            if(payloadData.id==0){
                 query = {
                    $or: [{sender: userId, receiver:payloadData.anotherUserId}, {receiver: userId,sender:payloadData.anotherUserId}],
                   // _id:{$lt:mongoose.Types.ObjectId(payloadData.id)}
                };
            }
            else{
                 query = {
                    $or: [{sender: userId, receiver:payloadData.anotherUserId}, {receiver: userId,sender:payloadData.anotherUserId}],
                    _id:{$lt:mongoose.Types.ObjectId(payloadData.id)}
                };
            }

            // var skip=payloadData.skip *100;
            var options = {sort:{sendTime:1},limit:99};

            var setData = {
            };

            Service.ChatService.getChats(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    date= result;
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,date);
        }
    })
};


var guessImage = function(payloadData,callback){
var userId;
var data;
    var mongoose = require('mongoose');

    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        getQuestions:['getId',function(cb){
            var query = {
                $match: {
                    $and: [{gameId: mongoose.Types.ObjectId(payloadData.gameId)}, {isDeleted: false}]
                }
        };
            var dataToGet = {
                $sample: { size: 5 }
            };
            console.log("====================a===================",query);

            Service.ImageGameService.getImagesAgg(query,dataToGet,{},{},function(err,result){
                if(err){
                    cb(null)
                }
                else{
                    data = result;
                    cb(null)
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data);
        }
    })
};



var testingPush = function(payloadData,callback){
    var userId;
    var data;

    var message = {message:"testing push"};
    var payload = "Request Rejected ";
    var deviceToken = "ff7c85c9a054e439b6e58257dedafd590249f7e4b8e82c6fa2a83646866eaf2c";
        NotificationManager.sendIosPushNotification(deviceToken, message, payload, function(err,result){
            if(err){
                callback(err)
            }
            else{
                callback(null,"success");
            }
        });
};



var updateDeviceToken = function(payloadData,callback){


    var userId;
    async.auto({
        getUserId:function(cb){

            var projections = {_id:1};
            var options = {lean:true};
            var getCriteria = {
                accessToken:payloadData.accessToken,
            }
            Service.CustomerService.getCustomer(getCriteria,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    if(result.length){
                        userId = result[0]._id;
                        cb(null);
                    }else{
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    }
                }
            })
        },
        updateToken:['getUserId',function(cb){

            var query = {
                _id:userId
            };

            var options = {lean:true};
            var setData = {
                deviceToken : payloadData.deviceToken,
                deviceType: payloadData.deviceType
            };

            Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    cb(null);
                }
            })

        }],
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,{message:"Token Updated"});
        }
    })
};


var sendImageMessage = function(payLoadData,callback){

    var id;
    var profilePic={};
    var video;
    var ToDeviceToken;
    var ToDeviceType;
    var chatId;
    var data;

    console.log("=========================",payLoadData)

    async.auto({
        accessToken:function (cb) {
            var getCriteria = {
                accessToken:payLoadData.accessToken,
                isBlocked:"false"
            };
            Service.CustomerService.getCustomer(getCriteria, {_id:1,userName:1}, {lean:true}, function (err, data) {
                if (err) {
                    cb({errorMessage: 'DB Error: ' + err})
                } else {
                    //console.log(data);
                    if (data && data.length > 0) {
                        id= data[0]._id;
                        cb(null);
                    } else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS)
                    }
                }
            });
        },
        imageUpload:['accessToken',function(cb){

            var pic = id + "chatMessage" + +new Date().getTime();
            UploadManager.uploadFileToS3WithThumbnail(payLoadData.image, pic,payLoadData.message_type, function(err,result){

                if(err){
                    console.log("here video err",err);
                    cb()
                }
                else{
                    if(payLoadData.message_type==1){
                        profilePic= Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                    }
                    else{
                        profilePic= Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        console.log("=========profilePic00000========",profilePic)
                    }
                 //    profilePic.original= Config.awsS3Config.s3BucketCredentials.s3URL +result.original;
                    //  console.log("profilepath====================",profilePic.original);
                    cb(null);
                }
            })
        }],
        videoUpload:['accessToken',function(cb){
            if(payLoadData.message_type==2){
                var pic = id + "chatMessage_video" + +new Date().getTime();
                UploadManager.uploadFileToS3WithThumbnail(payLoadData.thumbnail, pic,0, function(err,result){

                    if(err){
                        console.log("here err",err);
                        cb()
                    }
                    else{
                        // profilePic.original= "https://s3-us-west-2.amazonaws.com/timjdating/"+result.original;
                        video= Config.awsS3Config.s3BucketCredentials.s3URL+result.original;
                        //  console.log("profilepath====================",profilePic.original);
                        cb(null);
                    }
                })
            }
            else{
                cb(null);
            }
        }],
        saveSms:['imageUpload','videoUpload',function(cb){
            if(payLoadData.message_type==1){
                data = {
                    receiver:payLoadData.to,
                    sender:id,
                    message:profilePic,
                    sendTime:payLoadData.timeStamp,
                    message_type:payLoadData.message_type
                };
            }
            else{
                data = {
                    receiver:payLoadData.to,
                    sender:id,
                    message:profilePic,
                    sendTime:payLoadData.timeStamp,
                    message_type:payLoadData.message_type,
                    thumbnail:video
                };
            }
            console.log("===================",data);
            SocketManager.saveMeassage(data,function(err,result){

                if(err){
                    cb(err)
                }
                else{
                    console.log("==================image=================",result)
                    cb(null);
                }
            })

        }],
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,{});
        }
    })
};


var gameStatus = function(payLoadData,callback){

    var userId;
    var data;
    var mongoose = require('mongoose');


    async.auto({
        getUserId:function(cb){

            var projections = {_id:1};
            var options = {lean:true};
            var getCriteria = {
                accessToken:payLoadData.accessToken
            };
            Service.CustomerService.getCustomer(getCriteria,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    if(result.length){
                        userId = result[0]._id;
                        console.log("=======================",userId);
                        cb(null);
                    }else{
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    }
                }
            })
        },
        getGameStatus:['getUserId',function(cb){

            var query = {
                $or: [{sender: mongoose.Types.ObjectId(userId)}, {receiver: mongoose.Types.ObjectId(userId)}],
               // $or: [{sender: mongoose.Types.ObjectId(userId)}, {receiver: mongoose.Types.ObjectId(userId)}],

                matchStatus:{$nin:['CLOSE','TIE']}
            };

            var options = {sort:{requestTime:-1}};
            var setData = {
            };

            Service.RequestGamesService.getRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    data = result;
                    console.log("==============data=================",result)
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data);
        }
    })

};



var rewards = function(payLoadData,callback){

    var userId;
    var data;
    var mongoose = require('mongoose');


    async.auto({
        getUserId:function(cb){

            var projections = {_id:1};
            var options = {lean:true};
            var getCriteria = {
                accessToken:payLoadData.accessToken
            };
            Service.CustomerService.getCustomer(getCriteria,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    if(result.length){
                        userId = result[0]._id;
                        console.log("=======================",userId);
                        cb(null);
                    }else{
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    }
                }
            })
        },
        getGameRewards:['getUserId',function(cb){

            var query = {
                $or: [{sender: mongoose.Types.ObjectId(userId)}, {receiver: mongoose.Types.ObjectId(userId)}],
                matchStatus:'CLOSE',
                iAmSnake:false
            };

            var options = {sort:{gameEndTime:1}};
            var setData = {
                winUser:1,
                lossUser:1,
                wager:1,
                games:1
            };
            Service.RequestGamesService.getRequestGamesRewards(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    data = result;
                    console.log("==============data=================",result);
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data);
        }
    })

};




var pending = function(payLoadData,callback){

    var userId;
    var data;
    var mongoose = require('mongoose');


    async.auto({
        getUserId:function(cb){

            var projections = {_id:1};
            var options = {lean:true};
            var getCriteria = {
                accessToken:payLoadData.accessToken
            };
            Service.CustomerService.getCustomer(getCriteria,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    if(result.length){
                        userId = result[0]._id;
                        console.log("=======================",userId);
                        cb(null);
                    }else{
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    }
                }
            })
        },
        getGameRewards:['getUserId',function(cb){

            var query = {
              //  $or: [{sender: mongoose.Types.ObjectId(userId)}, {receiver: mongoose.Types.ObjectId(userId)}],
                lossUser:userId,
                matchStatus:'CLOSE',
                iAmSnake:true
            };

            var options = {sort:{gameEndTime:1}};
            var setData = {
                winUser:1,
                lossUser:1,
                wager:1,
                games:1
            };
            Service.RequestGamesService.getRequestGamesRewards(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    data = result;
                    console.log("==============data=================",result);
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data);
        }
    })

};



var accessTokenLogin = function(payLoadData,callback){

    var userId;
    var data;
    var mongoose = require('mongoose');

    async.auto({
        getUserId:function(cb){

            var projections = {_id:1};
            var options = {lean:true};
            var getCriteria = {
                accessToken:payLoadData.accessToken
            };
            Service.CustomerService.getCustomer(getCriteria,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    if(result.length){
                        userId = result[0]._id;
                        console.log("=======================",userId);
                        cb(null);
                    }else{
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    }
                }
            })
        },
        getGame:['getUserId',function(cb){

            var query = {
                //  $or: [{sender: mongoose.Types.ObjectId(userId)}, {receiver: mongoose.Types.ObjectId(userId)}],
                lossUser:mongoose.Types.ObjectId(userId),
                matchStatus:'CLOSE',
                payWager:false,
                iAmSnake:false
            };

            var options = {};
            console.log("==================query=================",query);
            var setData = {
            };
            Service.RequestGamesService.getRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    data = result;
                    console.log("==============data========query=========",result,query);
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null,data);
        }
    })

};




var testData = function(payload,callback){
    console.log(".......sdfvdv.df..........",payload);
    NotificationManager.sendAndroidPushNotification(payload.token,"dfsd",function(err,result){
        console.log(".......result");
    })
};



var getImageGames =function(payloadData,callback){

    var userId;
    var imageGame;
    async.auto({
        getAdminId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result[0]._id;
                    //  console.log(adminType)
                    cb(null);
                }
            })
        },
        getImageData: ['getAdminId',function(cb){
            var query={
                gameId:payloadData.gameId,
                isDeleted:false
            };
            Service.ImageGameService.getImageService(query,{},{lean:true},function(err,result){
                if(err){
                    cb(err);
                }
                else{
                    imageGame = result;
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err)
        }
        else{
            callback(null,imageGame);
        }
    })

};



var addCard = function (payloadData, callback) {

    var data1;
    var customerId;
    var stripeId;
    var soruceId;
    var userId;
    stripeId = payloadData.stripeId

    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED);
                    }
                }
            });
        },
        //createToken:['checkForAccessToken',function(cb){
        //    console.log("====================here success===================");
        //    stripe.tokens.create({
        //        card: {
        //            "number": payloadData.cardNumber,
        //            "exp_month": payloadData.month,
        //            "exp_year": payloadData.year,
        //            "cvc": payloadData.cvv
        //        }
        //    }, function(err,result){
        //        if(err){
        //            console.log("====================error===================",err.raw.message);
        //            cb(err.raw.message)
        //        }else{
        //            stripeId = result.id;
        //            console.log("...................stripeId.......................",stripeId);
        //            cb(null);
        //        }
        //    });
        //}],
        createClient:['checkForAccessToken',function(cb){
            stripe.customers.create({
                description: 'Customer for ' + userId,
                source: stripeId // obtained with Stripe.js
            }, function (err, customer) {
                if (err) {
                    cb(err.raw.message);
                    console.log("=====================err================",err)
                } else {
                    customerId = customer.id;
                    soruceId = customer.default_source;

                    console.log("..................console.log(...............",customerId);

                    console.log("..................soruceId.log(...............",soruceId);
                    cb(null);

                }
            });
        }],
        list:['createClient',function(cb){
            var query={
                _id:userId
            }
            var setQuery={
                $push:{
                    cards:{
                        name:payloadData.name,
                        cardNumber:payloadData.cardNumber,
                     //   month:payloadData.month,
                       // year:payloadData.year,
                        isDefault:payloadData.defaut,
                       // cvv:payloadData.cvv,
                        soruceId:soruceId,
                        customerId:customerId
                    }
                }
            }
            var options={new:true};
            Service.CustomerService.updateCustomer(query,setQuery,options,function(err,data){
                if(err){
                    cb(err)
                }
                else{
                    cb(null)
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null);
        }
    })
};


var deleteCard = function (payloadData, callback) {

    var userId;
    var cardData;
    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        userId = result[0]._id;
                        cb();
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED);
                    }
                }
            });
        },
        updateCard:['checkForAccessToken',function(cb){
            var query={
                _id:userId,
                cards: { $elemMatch: { _id: payloadData.cardId }},

            }
            var setQuery={
                'cards.$.isDeleted':true

            }
            var options={new:true}
            Service.CustomerService.updateCustomer(query,setQuery,options,function(err,data){
                if(err){
                    cb(err)
                }
                else{
                    cardData=data;
                    cb(null)
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, cardData);
        }

    })

};



var getCards = function (payloadData, callback) {

    var cardData=[];
    var data1;

    async.auto({
        checkForAccessToken: function (cb) {

            var criteria = {
                accessToken: payloadData.accessToken
            };

            var projection={
                _id:0,
                cards: 1
            };

            var options={ lean:true };
            Service.CustomerService.getCustomer(criteria, projection, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {

                    if (result.length) {
                        if(result[0].cards){
                            console.log("=============================================",result[0].cards.length);

                            for(var i= 0,j=0;i<result[0].cards.length;i++){
                                (function(i){

                                    if(result[0].cards[i].isDeleted==false){
                                        cardData[j] = result[0].cards[i];
                                        j++;
                                    }
                                    if(i==result[0].cards.length-1){
                                        cb(null);
                                    }
                                }(i))
                            }

                        }
                        else{
                        }
                        cb(null);
                    }
                    else {
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED);
                    }
                }
            });
        }
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null,cardData);
        }

    })
};



var payWager = function(payloadData,callback){
    var userId;
    var price=10;
    var trxId=null;
    var customerId;
    var soruceId;
    async.auto({
        getId:function(cb){
            getIdByToken(payloadData.accessToken,function(err,result){
                if(err){
                    cb(err);
                }else{
                    userId = result;
                    cb(null)
                }
            })
        },
        getPriceOfWager:['getId',function(cb){
            var query = {
                _id:payloadData.requestId
            };
            var options = {lean:true};
            var setData = {
            };
            Service.RequestGamesService.getRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                  //  console.log("====================price=============",result);
                    if(result[0].wager.cost)
                    price= result[0].wager.cost;
                    cb(null)
                }
            })
        }],
        getCardDetails:['getId',function(cb){
            var query={
                _id:userId,
                'cards._id':payloadData.cardId
            };
            var projection={
                "cards.$":1
            };
            Service.CustomerService.getCustomer(query,projection,{lean:true},function (err,data) {
                if(err){
                    cb(err)
                }
                else{

                //    console.log("......",data,"...........gggggggggg.....")
                    customerId=data[0].cards[0].customerId;
                    soruceId = data[0].cards[0].soruceId;
                    cb(null)
                }
            })

        }],
        makePayment:['getCardDetails','getPriceOfWager',function(cb){
        console.log("==========price============",price)
            stripe.charges.create({
                    amount: price * 100, // smallest unit i.e. pesos
                    currency: 'usd',
                    source: soruceId,
                    customer: customerId,
                    description: "Charges for paying wager"
                },
                function (err, stripeResponse) {
                    if (err) {
                        console.log("=========err=============",err)
                        cb(err.raw.message);
                    } else {
                        trxId = stripeResponse.id;
                        console.log("=================trans id==========",trxId)
                        cb(null)

                    }
                })
        }],
        markStatusToPaid:['makePayment',function(cb){
            var query = {
                _id:payloadData.requestId
            };
            var options = {lean:true};
            var setData = {
                payWager:true,
                transactionId:trxId
            };
            Service.RequestGamesService.updateRequestGames(query,setData,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    console.log("====================price=============",result);
                    cb(null)
                }
            })
        }],
        reduceSnakeCount:['makePayment',function(cb){
            var query = {
                _id:userId
            };
            var dataToSet = {
                $inc:{snake:-1}
            };
            Service.CustomerService.updateCustomer(query,dataToSet,{},function(err,result){
                if(err){
                    cb(err)
                }
                else{
                    cb(null);
                }
            })
        }]
    },function(err,result){
        if(err){
            callback(err);
        }else{
            callback(null);
        }
    })
};


module.exports = {
    createCustomer: createCustomer,
    logoutCustomer: logoutCustomer,
    getAllGames:getAllGames,
    loginCustomerViaFacebook: loginCustomerViaFacebook,
    loginCustomer: loginCustomer,
    getAllFriends:getAllFriends,
    UserDetails:UserDetails,
    explore:explore,
    getAllWager:getAllWager,
    requestNewGames:requestNewGames,
    profileCompleted:profileCompleted,
    getIdByToken:getIdByToken,
    updateSerachProfile:updateSerachProfile,
    updateAllFbFriends:updateAllFbFriends,
    winnerGames:winnerGames,
    loseGames:loseGames,
    imSnake:imSnake,
    tieGame:tieGame,
    rematchGame:rematchGame,
    changeWager:changeWager,
    requestAccepted:requestAccepted,
    requestRejected:requestRejected,
    uploadImage0:uploadImage0,
    logout:logout,
    deleteImage:deleteImage,
    checkRegisteredUser:checkRegisteredUser,
    sendToOnGoing:sendToOnGoing,
    blockOngoing:blockOngoing,
    getAllChat:getAllChat,
    getIndivisualChat:getIndivisualChat,
    updateTempFriends:updateTempFriends,
    guessImage:guessImage,
    testingPush:testingPush,
    updateDeviceToken:updateDeviceToken,
    sendImageMessage:sendImageMessage,
    gameStatus:gameStatus,
    testData:testData,
    getImageGames:getImageGames,
    getCards: getCards,
    addCard: addCard,
    deleteCard: deleteCard,
   // changeDefaultCard: changeDefaultCard,
    rewards:rewards,
    pending:pending,
    payWager:payWager,
    accessTokenLogin:accessTokenLogin
};