/**
 * Created by shahab on 10/7/15.
 */
module.exports  = {
    DriverController : require('./DriverController'),
    AdminController : require('./AdminController'),
    AppVersionController : require('./AppVersionController'),
    CustomerController : require('./CustomerController')
};