'use strict';
/**
 * Created by shahab on 12/7/15.
 */
var mongoose = require('mongoose');
var Config = require('../Config');
var SocketManager = require('../Lib/SocketManager');
var Service = require('../Services');
var async = require('async');

//Connect to MongoDB
mongoose.connect(Config.dbConfig.mongo.URI, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});

exports.games = function (callback) {
    var game1 = {
        gameName: 'Image Game',
        gameImages:{
        original:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg" ,
        thumbnail:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg"
    },
        gameType:Config.APP_CONSTANTS.DATABASE.GAMETYPE.INTERACTIVE,
        uniqueName:"Image"
    };
    var game2 = {
        gameName: 'TIC-TAK-TOE',
        gameImages:{
            original:"https://s3-us-west-2.amazonaws.com/winmos3/Screen+Shot+2016-11-28+at+7.58.30+PM.png" ,
            thumbnail:"https://s3-us-west-2.amazonaws.com/winmos3/Screen+Shot+2016-11-28+at+7.58.30+PM.png"
        },
        gameType:Config.APP_CONSTANTS.DATABASE.GAMETYPE.TURNBASE,
        uniqueName:"Tic"
    };
    var game3 = {
        gameName: 'PAPER-SCISSOR-STONE',
        gameImages:{
            original:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg" ,
            thumbnail:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg"
        },
        gameType:Config.APP_CONSTANTS.DATABASE.GAMETYPE.TURNBASE,
        uniqueName:"Paper"
    };
    var game4 = {
        gameName: 'IMAGE GUESS',
        gameImages:{
            original:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg" ,
            thumbnail:"https://s3-us-west-2.amazonaws.com/timjdating/profileThumb_5807260e152c1b13238af700.jpg"
        },
        gameType:Config.APP_CONSTANTS.DATABASE.GAMETYPE.INTERACTIVE,
        uniqueName:"ImageAll"
    };
    async.parallel([
        function (cb) {
            insertDataGames(game1.gameName, game1, cb)
        },
        function (cb) {
            insertDataGames(game2.gameName, game2, cb)
        },
        function (cb) {
            insertDataGames(game3.gameName, game3, cb)
        },
        function (cb) {
            insertDataGames(game4.gameName, game4, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })
};


exports.bootstrapAdmin = function (callback) {
    var adminData1 = {
        email: 'shumigupta04@gmail.com',
        password: '1e7eebb19ca71233686f26a43bbc18a9',
        adminType:"0",
        name: 'shumi'
    };
    var adminData2 = {
        email: 'shumigupta@hotmail.com',
        password: '1e7eebb19ca71233686f26a43bbc18a9',
        adminType:"0",
        name: 'shumi'
    };
    async.parallel([
        function (cb) {
            insertData(adminData1.email, adminData1, cb)
        },
        function (cb) {
            insertData(adminData2.email, adminData2, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })
};
exports.bootstrapAppVersion = function (callback) {
    var appVersion1 = {
        latestIOSVersion: '100',
        latestAndroidVersion: '100',
        criticalAndroidVersion: '100',
        criticalIOSVersion: '100',
        appType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
    };
    var appVersion2 = {
        latestIOSVersion: '100',
        latestAndroidVersion: '100',
        criticalAndroidVersion: '100',
        criticalIOSVersion: '100',
        appType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER
    };


    async.parallel([
        function (cb) {
            insertVersionData(appVersion1.appType, appVersion1, cb)
        },
        function (cb) {
            insertVersionData(appVersion2.appType, appVersion2, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished For App Version');
    })


};

function insertVersionData(appType, versionData, callback) {
    var needToCreate = true;
    async.series([
        function (cb) {
        var criteria = {
            appType: appType
        };
        Service.AppVersionService.getAppVersion(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.AppVersionService.createAppVersion(versionData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + appType);
        callback(err, 'Bootstrapping finished For Admin Data')
    })
}

function insertData(email, adminData, callback) {
    var needToCreate = true;
    async.series([function (cb) {
        var criteria = {
            email: email
        };
        Service.AdminService.getAdmin(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.AdminService.createAdmin(adminData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + email);
        callback(err, 'Bootstrapping finished')
    })
}



function insertDataGames(gameName, adminData, callback) {
    var needToCreate = true;
    async.series([function (cb) {
        var criteria = {
            gameName: gameName
        };
        Service.GamesService.getGames(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.GamesService.createGames(adminData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
       // console.log('Bootstrapping finished for ' + email);
        callback(err, 'Bootstrapping finished')
    })
}

//Start Socket Server
exports.connectSocket = SocketManager.connectSocket;


