'use strict';

var Models = require('../Models');

//Get Users from DB
var getGames = function (criteria, projection, options, callback) {
    Models.Games.find(criteria, projection, options, callback);
};


//Insert User in DB
var createGames = function (objToSave, callback) {
    new Models.Games(objToSave).save(callback)
};

//Update User in DB
var updateGames = function (criteria, dataToSet, options, callback) {
    Models.Games.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteGames = function (criteria, callback) {
    Models.Games.findOneAndRemove(criteria, callback);
};


module.exports = {
    deleteGames:deleteGames,
    updateGames:updateGames,
    createGames:createGames,
    getGames:getGames
};

