'use strict';

var Models = require('../Models');

//Get Users from DB
var getRequestMessage = function (criteria, projection, options, callback) {
    Models.RequestMessage.find(criteria, projection, options, callback);
};


//Insert User in DB
var createRequestMessage = function (objToSave, callback) {
    new Models.RequestMessage(objToSave).save(callback)
};

//Update User in DB
var updateRequestMessage = function (criteria, dataToSet, options, callback) {
    Models.RequestMessage.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteRequestMessage = function (criteria, callback) {
    Models.RequestMessage.findOneAndRemove(criteria, callback);
};


module.exports = {
    deleteRequestMessage:deleteRequestMessage,
    updateRequestMessage:updateRequestMessage,
    createRequestMessage:createRequestMessage,
    getRequestMessage:getRequestMessage
};

