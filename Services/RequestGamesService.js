'use strict';

var Models = require('../Models');

//Get Users from DB
var getRequestGames = function (criteria, projection, options, callback) {
    Models.RequestGames.find(criteria, projection, options)
        .populate({path:'games',select:'gameName gameImages uniqueName'})
        .populate({path:'sender',select:'name facebookId'})
        .populate({path:'receiver',select:'name facebookId'})
        .populate({path:'wager',select:'name images description cost'})
       // .populate({path:'games',select:'gameName gameImages'})
        .exec(function(error, results) {
            callback(error,results);
        });
};


var getRequestGamesRewards = function (criteria, projection, options, callback) {
    Models.RequestGames.find(criteria, projection, options)
        .populate({path:'games',select:'gameName gameImages uniqueName'})
        .populate({path:'winUser',select:'name facebookId'})
        .populate({path:'lossUser',select:'name facebookId'})
        .populate({path:'wager',select:'name images description referralCode1 referralCode2 cost'})
        // .populate({path:'games',select:'gameName gameImages'})
        .exec(function(error, results) {
            callback(error,results);
        });
};

//Insert User in DB
var createRequestGames = function (objToSave, callback) {
    new Models.RequestGames(objToSave).save(callback)
};

//Update User in DB
var updateRequestGames = function (criteria, dataToSet, options, callback) {
    Models.RequestGames.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteRequestGames = function (criteria, callback) {
    Models.RequestGames.findOneAndRemove(criteria, callback);
};


module.exports = {
    deleteRequestGames:deleteRequestGames,
    updateRequestGames:updateRequestGames,
    createRequestGames:createRequestGames,
    getRequestGames:getRequestGames,
    getRequestGamesRewards:getRequestGamesRewards
};

