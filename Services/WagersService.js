'use strict';

var Models = require('../Models');

//Get Users from DB
var getWages = function (criteria, projection, options, callback) {
    Models.Wages.find(criteria, projection, options, callback);
};

var getWagesDistinct = function (criteria, projection, options, callback) {
    Models.Wages.distinct(criteria, projection, options, callback);
};

//Insert User in DB
var createWages = function (objToSave, callback) {
    new Models.Wages(objToSave).save(callback)
};

//Update User in DB
var updateWages = function (criteria, dataToSet, options, callback) {
    Models.Wages.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteWages = function (criteria, callback) {
    Models.Wages.findOneAndRemove(criteria, callback);
};

var getWagesByadmin = function (criteria,projection,options, callback) {
    Models.Wages.find(criteria,projection,options)
        //.populate({path:'createdBy',match: { "isBlocked": { $eq: false }},select:'name '})
        //.populate({path:'redeemedBy',match: { "isBlocked": { $eq: false }},select:'name '})
        .populate('createdBy' ,'name')
        .populate('redeemedBy' ,'name')
        
        .exec(function(error, results) {
            callback(error,results);
        });
};

module.exports = {
    deleteWages:deleteWages,
    updateWages:updateWages,
    createWages:createWages,
    getWages:getWages,
    getWagesByadmin:getWagesByadmin
};

