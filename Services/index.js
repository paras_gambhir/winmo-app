/**
 * Created by prince on 10/7/15.
 */
module.exports = {
    CustomerService : require('./CustomerService'),
    ChatService : require('./ChatService'),
    AdminService : require('./AdminService'),
    AppVersionService : require('./AppVersionService'),
    ImagesService:require('./ImagesService'),
    GamesService:require('./GamesServices'),
    WagerService:require('./WagersService'),
    RequestGamesService:require('./RequestGamesService'),
    RequestMessageService:require('./RequestMessageService'),
    ImageGameService:require('./ImageGameService')
};