'use strict';

var Models = require('../Models');

//Get Users from DB
var getImageService = function (criteria, projection, options, callback) {
    Models.ImageGame.find(criteria, projection, options, callback);
};


//Insert User in DB
var createImageService = function (objToSave, callback) {
    new Models.ImageGame(objToSave).save(callback)
};

//Update User in DB
var updateImageService = function (criteria, dataToSet, options, callback) {
    Models.ImageGame.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteImageService = function (criteria, callback) {
    Models.ImageGame.findOneAndRemove(criteria, callback);
};

var getImagesAgg = function (criteria,dataToSet,project, options, callback) {
    Models.ImageGame.aggregate(
        [
            criteria,
            dataToSet
        ])
        //.populate({path:'from',select:'name profilePicURL'})
        .exec(function(error, result) {
            console.log("============a=================",result);
            //Models.Chats.populate(result,{path:'from',select:'name profilePicURL'} ,function(err, populatedTransactions){
                callback(error,result);
            //})
        });
}

module.exports = {
    deleteImageService:deleteImageService,
    updateImageService:updateImageService,
    createImageService:createImageService,
    getImageService:getImageService,
    getImagesAgg:getImagesAgg
};

