'use strict';

var Models = require('../Models');

//Get Users from DB
var getChats = function (criteria, projection, options, callback) {
    Models.Chats.find(criteria, projection, options, callback);
};


//Insert User in DB
var createChats = function (objToSave, callback) {
    new Models.Chats(objToSave).save(callback)
};

//Update User in DB
var updateChats = function (criteria, dataToSet, options, callback) {
    Models.Chats.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteChats = function (criteria, callback) {
    Models.Chats.findOneAndRemove(criteria, callback);
};


var getCustomerByPopulate = function (criteria,projection,options, callback) {
    Models.Customers.find(criteria,projection,options)
      //  .populate({path:'receiver',select:'name '})
       // .populate({path:'message',select:'name '})
        .exec(function(error, results) {
            callback(error,results);
        });
};


var getAllChating = function (criteria,dataToSet,project, options, callback) {
    Models.Chats.aggregate(
        [
            criteria,
            options,
            dataToSet,
            project,
        ])
        .exec(function(error, result) {
            Models.Chats.populate(result,{path:'sender',select:'name '},function(err,result123){
                callback(error,result123);
            })
        });
}

var getAllChating1 = function (criteria,dataToSet,project, options, callback) {
    Models.Chats.aggregate(
        [
            criteria,
            options,
            dataToSet,
            project,
        ])
        .exec(function(error, result) {
            Models.Chats.populate(result,{path:'receiver',select:'name '},function(err,result123){
                callback(error,result123);
            })
        });
}

module.exports = {
    deleteChats:deleteChats,
    updateChats:updateChats,
    createChats:createChats,
    getChats:getChats,
    getCustomerByPopulate:getCustomerByPopulate,
    getAllChating:getAllChating,
    getAllChating1:getAllChating1
};

