'use strict';

var Models = require('../Models');

//Get Users from DB
var getImages = function (criteria, projection, options, callback) {
    Models.Images.find(criteria, projection, options, callback);
};


//Insert User in DB
var createImages = function (objToSave, callback) {
    new Models.Images(objToSave).save(callback)
};

//Update User in DB
var updateImages = function (criteria, dataToSet, options, callback) {
    Models.Images.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteImages = function (criteria, callback) {
    Models.Images.findOneAndRemove(criteria, callback);
};


module.exports = {
    deleteImages:deleteImages,
    updateImages:updateImages,
    createImages:createImages,
    getImages:getImages
};

